# Console Development Assessment

PSVita game developed using AbertayFramework and Box2D.

The player controls an Ooze covered asteroid using the front touch screen to apply forces to the asteroid to collect more Ooze scattered around the universe. They must dodge the hazards of the universe as they navigate the gravitational pull of the planets and the sun in their search for a fertile planet. Each finger generates a solar wind to push the asteroid. 

## CC by 3.0 - http://creativecommons.org/licenses/by-sa/3.0/

	Bleed - Explosions
	http://opengameart.org/content/simple-explosion-bleeds-game-art

	Tatermand - asteroids, planets and space background
	http://opengameart.org/content/space-game-art-pack-extended

	Trevor Lentz - Background music
	http://opengameart.org/content/hero-immortal

	Michel Baradari - explosion sound effect
	http://opengameart.org/content/2-high-quality-explosions
