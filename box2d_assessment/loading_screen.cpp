#include "loading_screen.h"

#include <system/platform.h>
#include <graphics/sprite_renderer.h>

#include "screen_manager.h"

const float bg_height_offset = 200;
const UInt32 bg_colour = 0x805D1372;
const UInt32 dodad_colour = 0xFF79BE00;

const float dodad_size = 64;

const float loading_percentage_y = 100;
const float loading_description_y = 400;


void LoadingScreen::Init(){
	background_.set_position(platform_.width() / 2, platform_.height() / 2, -1.f);
	background_.set_width(platform_.width());
	background_.set_height(platform_.height() - bg_height_offset);
	background_.set_colour(bg_colour);

	dodad_.set_position(platform_.width() / 2, platform_.height() / 2, -1.0f);
	dodad_.set_width(dodad_size);
	dodad_.set_height(dodad_size);
	dodad_.set_colour(dodad_colour);
	percentage_ = 0;
	description_ = "";
}

void LoadingScreen::CleanUp(){
	
}

void LoadingScreen::Update(float dt){
	std::vector<GameScreen* >::iterator screen = screen_manager_->GetIterTopScreen();
	screen--;
	if (!(*screen)->IsLoading(percentage_, description_)){
		screen_manager_->PopScreen();
	}
	dodad_.set_rotation(dodad_.rotation() + 0.1f);
}

void LoadingScreen::Render(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font_){
	sprite_renderer->DrawSprite(background_);
	sprite_renderer->DrawSprite(dodad_);
	font_->RenderText(sprite_renderer, abfw::Vector3(platform_.width() / 2, loading_percentage_y, -1.0f), 1.5f, 0xffffffff, abfw::TJ_CENTRE, "Loading... %.0f %", percentage_ * 100);
	font_->RenderText(sprite_renderer, abfw::Vector3(platform_.width() / 2, loading_description_y, -1.0f), 1.5f, 0xffffffff, abfw::TJ_CENTRE, "Loading: %s", description_.c_str());
}