#ifndef _LEVEL_TWO_H
#define _LEVEL_TWO_H
#include "level.h"

class LevelTwo :public Level{
public:
	LevelTwo(abfw::Platform& platform, SharedAssets &shared_assets) :
		Level(platform, shared_assets){
	}
protected:
	// Load any suplemental assets, or audio
	void LoadLevel() override;
	// Build the scene, portal ship initialization etc.
	void BuildLevel() override;
	// Clean up any suplemental assets, or audio
	void CleanUpLevel() override;

	abfw::Texture *t_sun_;
	abfw::Texture *t_red_giant_;
	abfw::Texture *t_gas_giant_;
	abfw::Texture *t_ice_giant_;
};

#endif