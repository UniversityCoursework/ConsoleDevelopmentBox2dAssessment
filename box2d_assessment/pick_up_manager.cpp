#include "pick_up_manager.h"

void PickUpManager::CleanUp() {

	// Asteroids
	for (PickUp_It pickup = pickups_.begin(); pickup != pickups_.end(); ++pickup) {
		delete(*pickup);
		(*pickup) = nullptr;
	}
	pickups_.clear();
}

void PickUpManager::Update(float dt) {
	for (PickUp_It pickup = pickups_.begin(); pickup != pickups_.end();) {
		if ((*pickup)->needs_deleted()) {
			delete (*pickup);
			(*pickup) = nullptr;
			pickup = pickups_.erase(pickup);
		}
		else {			
			++pickup;
		}
	}
}
void PickUpManager::Render(abfw::SpriteRenderer *renderer) {
	for (PickUp_It pickup = pickups_.begin(); pickup != pickups_.end(); ++pickup) {
		if (!(*pickup)->needs_deleted()) {
			(*pickup)->Render(renderer);
		}
	}
}

std::vector<PickUp*> &PickUpManager::GetPickUps() {
	return pickups_;
}

void PickUpManager::AddPickUp(PickUp * spawner) {	
	pickups_.push_back(spawner);
}
