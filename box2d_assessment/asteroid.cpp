#include "asteroid.h"

#include "Box2D\Box2D.h"
#include "box2d_helpers.h"

const float damage_scale_factor_ = 10.0f;
const float angular_damping_ = 0.1f;
const float density_ = 60.0f;
const float friction_ = 1.0f;
const float restitution_ = 0.0f;

const float explosion_scale_ = 2.0f;

const float frame_duration_ = 0.06f;
const int num_frames_ = 13;
const float uv_width_ = 0.0769f;
const float uv_offset_x_ = 0.0769f;

Asteroid::Asteroid() {
}

Asteroid::~Asteroid() {	
	DestroyBody();
}

Asteroid::Asteroid(b2World* world, abfw::Vector3 position, float radius, float collision_radius, b2Vec2 direction, abfw::Texture * base_texture, abfw::Texture * explosion_texture) {
	radius *= 2;
	sprite_.set_width(radius);
	sprite_.set_height(radius);
	sprite_.set_position(position);
	sprite_.set_texture(base_texture);
	
	explosion_sprite_.set_width(radius*explosion_scale_);
	explosion_sprite_.set_height(radius*explosion_scale_);
	explosion_sprite_.set_position(position);
	explosion_sprite_.set_texture(explosion_texture);

	explosion_sprite_.set_anim_frame_duration(frame_duration_);
	explosion_sprite_.set_num_frames(num_frames_);
	explosion_sprite_.set_uv_offset(abfw::Vector2(uv_offset_x_, 0));
	explosion_sprite_.set_uv_width(uv_width_);

	CreateDynamicBody(world, collision_radius);
	body_->ApplyForceToCenter(direction);	
	set_type(kAsteroid);
	is_alive_ = true;	
}

float Asteroid::GetDamage() {	
	return  body_->GetMass()*damage_scale_factor_*body_->GetLinearVelocity().Length();
}

void Asteroid::BlowUp() {
	if (is_alive()) {
		is_alive_ = false;		
		explosion_sprite_.set_position(BOX2D_ABFW_POS_X(body_->GetPosition().x), BOX2D_ABFW_POS_Y(body_->GetPosition().y), sprite_.position().z);		
	}		
}

void Asteroid::Update(float dt) {
	if (is_alive()) {
		sprite_.set_position(BOX2D_ABFW_POS_X(body_->GetPosition().x), BOX2D_ABFW_POS_Y(body_->GetPosition().y), sprite_.position().z);
		sprite_.set_rotation(-body_->GetAngle());
	}else if (explosion_sprite_.UpdateAnimation(dt)){		
		// animation ended so flag for deletion
		needs_deleted_ = true;
	} else {	// is just playing blow up animation so should no longer be active
		body_->SetActive(false);		
	}	
}

void Asteroid::Render(abfw::SpriteRenderer *sprite_renderer){
	if (is_alive()) {
		sprite_renderer->DrawSprite(sprite_);
	} else {
		sprite_renderer->DrawSprite(explosion_sprite_);
	}

}

void Asteroid::CreateDynamicBody(b2World * world, float collision_radius) {
	// dynamic body
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position = b2Vec2(ABFW_BOX2D_POS_X(sprite_.position().x), ABFW_BOX2D_POS_Y(sprite_.position().y));	
	bodyDef.angularDamping = angular_damping_;
	// add to world and store pointer to it
	body_ = world->CreateBody(&bodyDef);
	
	b2CircleShape circle;
	circle.m_p.SetZero();
	circle.m_radius = ABFW_BOX2D_SIZE(collision_radius);
	
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circle;
	fixtureDef.density = density_;
	fixtureDef.friction = friction_;
	fixtureDef.restitution = restitution_;

	fixtureDef.filter.categoryBits = kAsteroid;
	fixtureDef.filter.maskBits = kPlayer | kPlanet | kAsteroid | kPortal;

	body_->CreateFixture(&fixtureDef);	

	body_->SetUserData(this);
}
