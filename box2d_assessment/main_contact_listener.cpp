#include "main_contact_listener.h"

#include "game_object.h"
#include "planet.h"
#include "player.h"
#include "asteroid.h"
#include "portal.h"
#include "pick_up.h"

#ifdef _PLATFORM_VITA
void MainContactListener::Init(abfw::AudioManagerVita *audio_manager, GameSettings* game_settings, int pickup_id, int explosion_id) {
	audio_manager_ = audio_manager;
	pickup_id_ = pickup_id;
	explosion_id_ = explosion_id;
	game_settings_ = game_settings;
}

void MainContactListener::PlayPickUpSFX(){	
	int voice = audio_manager_->PlaySample(pickup_id_);
	abfw::VolumeInfo volume_info;
	volume_info.volume = game_settings_->sfx_volume();
	audio_manager_->SetSampleVoiceVolumeInfo(voice, volume_info);
}

void MainContactListener::PlayExplosionSFX(){
	int voice = audio_manager_->PlaySample(explosion_id_);
	abfw::VolumeInfo volume_info;
	volume_info.volume = game_settings_->sfx_volume();
	audio_manager_->SetSampleVoiceVolumeInfo(voice, volume_info);

}
#else 
void MainContactListener::Init(GameSettings* game_settings) {	
	game_settings_ = game_settings;
}

#endif

void MainContactListener::BeginContact(b2Contact* contact){
	b2Body* bodyA = contact->GetFixtureA()->GetBody();
	b2Body* bodyB = contact->GetFixtureB()->GetBody();

	// collision response
	GameObject* game_objectA = static_cast<GameObject*>(bodyA->GetUserData());
	GameObject* game_objectB = static_cast<GameObject*>(bodyB->GetUserData());

	// ship pickup
	if (game_objectA->type() == kPlayer && game_objectB->type() == kPickUp){
		Player* spaceship = static_cast<Player*>(game_objectA);
		PickUp* pickup = static_cast<PickUp*>(game_objectB);
		spaceship->set_score(spaceship->score() + pickup->GetPoints() - static_cast<int>(game_settings_->force_strength()));
#ifdef _PLATFORM_VITA
		PlayPickUpSFX();
#endif
	}
	if (game_objectB->type() == kPlayer && game_objectA->type() == kPickUp){
		Player* spaceship = static_cast<Player*>(game_objectB);
		PickUp* pickup = static_cast<PickUp*>(game_objectA);
		spaceship->set_score(spaceship->score() + pickup->GetPoints() - static_cast<int>(game_settings_->force_strength()));
#ifdef _PLATFORM_VITA
		PlayPickUpSFX();
#endif
	}
}

void MainContactListener::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse){
	
		b2Body* bodyA = contact->GetFixtureA()->GetBody();
		b2Body* bodyB = contact->GetFixtureB()->GetBody();

		// collision response
		GameObject* game_objectA = static_cast<GameObject*>(bodyA->GetUserData());
		GameObject* game_objectB = static_cast<GameObject*>(bodyB->GetUserData());

		// asteroid ship collision
		if (game_objectA->type() == kPlayer && game_objectB->type() == kAsteroid){
			Player* spaceship = static_cast<Player*>(game_objectA);
			Asteroid* asteroid = static_cast<Asteroid*>(game_objectB);	
			if (asteroid->is_alive()){				
				spaceship->take_damage(asteroid->GetDamage());
				asteroid->BlowUp();
#ifdef _PLATFORM_VITA
				PlayExplosionSFX();
#endif
			}
		}
		if (game_objectB->type() == kPlayer && game_objectA->type() == kAsteroid){
			Player* spaceship = static_cast<Player*>(game_objectB);
			Asteroid* asteroid = static_cast<Asteroid*>(game_objectA);
			if (asteroid->is_alive()){
				spaceship->take_damage(asteroid->GetDamage());
				asteroid->BlowUp();
#ifdef _PLATFORM_VITA
				PlayExplosionSFX();
#endif
			}
		}
		
		if (game_objectA->type() == kPortal && game_objectB->type() == kPlayer){
			Portal* portal = static_cast<Portal*>(game_objectA);
			portal->set_is_entered(true);
		}
		if (game_objectB->type() == kPortal && game_objectA->type() == kPlayer){
			Portal* portal = static_cast<Portal*>(game_objectB);
			portal->set_is_entered(true);
		}

		if (game_objectA->type() == kAsteroid &&game_objectB->type() != kAsteroid){
			Asteroid* asteroid = static_cast<Asteroid*>(game_objectA);			
			asteroid->BlowUp();
#ifdef _PLATFORM_VITA
			if (asteroid->is_alive()){
				PlayExplosionSFX();
			}
#endif
		}
		if (game_objectB->type() == kAsteroid &&game_objectA->type() != kAsteroid){
			Asteroid* asteroid = static_cast<Asteroid*>(game_objectB);
			asteroid->BlowUp();
#ifdef _PLATFORM_VITA
			if (asteroid->is_alive()){
				PlayExplosionSFX();
			}
#endif
		}

		// planet ship collision
		if (game_objectA->type() == kPlayer && game_objectB->type() == kPlanet){
			Player* spaceship = static_cast<Player*>(game_objectA);			
			if (spaceship->is_alive()){
				spaceship->take_damage(spaceship->health());
			}
		}
		if (game_objectB->type() == kPlayer && game_objectA->type() == kPlanet){
			Player* spaceship = static_cast<Player*>(game_objectB);
			if (spaceship->is_alive()){
				spaceship->take_damage(spaceship->health());
			}
		}		
}
