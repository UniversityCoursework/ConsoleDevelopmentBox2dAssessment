#ifndef _SHARED_ASSETS_H
#define _SHARED_ASSETS_H

#include <graphics\texture.h>

// relatively messy class,gives full acess to textures,
// ToDo: Limit access with accessors, seperating use and generation of the textures.
class SharedAssets{
public:
	abfw::Texture *t_background;
	abfw::Texture *t_portal;
	abfw::Texture *t_space_ship;
	abfw::Texture *t_pickup;
	void CleanUp(){
		delete t_background;
		t_background = nullptr;
		delete t_portal;
		t_portal = nullptr;
		delete t_space_ship;
		t_space_ship = nullptr;
		delete t_pickup;
		t_pickup = nullptr;
	}
};

#endif