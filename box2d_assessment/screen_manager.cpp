#include "screen_manager.h"

#include <maths\math_utils.h>

#ifdef _PLATFORM_VITA
void ScreenManager::Init(abfw::SonyControllerInputManager * controller_manager, abfw::TouchInputManager * touch_manager, Settings *settings, abfw::AudioManagerVita *audio_manager) {
	controller_manager_ = controller_manager;
	touch_manager_ = touch_manager;
	settings_ = settings;
	audio_manager_ = audio_manager;
}
#else
void ScreenManager::Init(abfw::SonyControllerInputManager* controller_manager, abfw::TouchInputManager* touch_manager,Settings *settings) {
	controller_manager_ = controller_manager;
	settings_ = settings;
	touch_manager_ = touch_manager;	
}
#endif

void ScreenManager::CleanUp() {
	// delete all the remaining screens in the vector
	for (Screen_It screen = screens_.begin(); screen != screens_.end(); screen++){
		(*screen)->CleanUp();
		delete (*screen);
		(*screen) = nullptr;
	}
	screens_.clear();
	delete settings_;
	settings_ = nullptr;
}
void ScreenManager::PushScreen(GameScreen* screen) {
	screens_.push_back(screen);
	screen->SetOwner(this);
	screen->Init();	
}
void ScreenManager::PopScreen(int number_of_screens) {
	// safer than actually popping off the screen in run time, 
	// will throw an error if you try to delete to many screens...
	Screen_It screen;
	screen = screens_.end()-1;
	for (int i = 0; (i < number_of_screens && i < screens_.size()); i++) {
		(*(screen-i))->set_needs_deleted(true);
		//PopScreen();
	}
}
void ScreenManager::PopScreen() {
	// safer than trying to pop screen at run time, as required the program to ensure it didnt run owning code.
	// will throw an error if you try to delete to many screens...
	screens_.back()->set_needs_deleted(true);
	/*if (!screens_.empty()){
		GameScreen* screen = screens_.back();		
		screen->CleanUp();		
		delete screen;
		screen = nullptr;		
		screens_.pop_back();
		if (!screens_.empty()){
			screens_.back()->Resume();
		}		
	}	*/
}

void ScreenManager::Update(float dt) {
	// update screens	
	int size = screens_.size();
	for(int i = 0; i < size; i++) {		
		screens_[i]->Update(dt);		
	}
	// delete any redundant screens
	for (Screen_It screen = screens_.begin(); screen != screens_.end();) {
		if((*screen)->needs_deleted()) {
			if(!screens_.empty()) {
				(*screen)->CleanUp();
				delete (*screen);
				(*screen) = nullptr;
				screen = screens_.erase(screen);
				if(!screens_.empty()) {
					screens_.back()->Resume();
				}				
			}
		} else {
			++screen;
		}
	}	
}
void ScreenManager::Render(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font_){
	// render only non hidden screens
	for (Screen_It screen = screens_.begin(); screen != screens_.end(); screen++){
		if((*screen)->GetState()!=kHidden){			
				(*screen)->Render(sprite_renderer, font_);			
		} 
	}
}

