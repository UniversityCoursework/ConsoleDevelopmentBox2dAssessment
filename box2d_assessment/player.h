#ifndef _SPACESHIP_H
#define _SPACESHIP_H

#include <graphics/sprite.h>
#include <graphics/texture.h>
#include <graphics/font.h>
#include <input/touch_input_manager.h>

#include <maths/vector3.h>

#include <array>

#include "game_object.h"
#include "animated_sprite.h"
#include "rectangle.h"
#include "stat_bar.h"


class Player : public GameObject{
public:
	Player(){};
	void Init(b2World* world, abfw::Vector3 positon, float radius, float collision_radius, abfw::Texture* texture, abfw::Texture* explosion_texture);

	void UpdateInput(abfw::TouchInputManager* touch_manager);
	void Update(float dt) override;

	void Render(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font);
	void RenderHud(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font);

	void BlowUp();

	float health(){ return health_; }	
	void set_health(float val) { health_ = val; }

	void take_damage(float val){		
		score_ -= val;
		if (score_ < 0){
			score_ = 0;
		}

		health_ -= val;
		if (health_ <= 0){
			health_ = 0;
			BlowUp();
		}
		
	}
	void set_is_alive(bool val){ is_alive_ = val; }
	bool is_alive() { return is_alive_; }

	void set_show_panels(bool val){ show_panels_ = val; }
	
	float force_strength(){ return force_strength_; }
	void set_force_strength(float val){ force_strength_ = val; }

	void set_score(int val){ score_ = val; }
	int score(){ return score_; }
	abfw::Vector3 position() { return sprite_.position(); }
protected:
	enum ForceDirection{		
		kT_Top = 0,
		kT_Left,
		kT_Right,
		kT_Bottom,		
	};
		
	abfw::Sprite sprite_;		
	StatBar health_bar_;
	AnimatedSprite explosion_sprite_;

	float health_;
	float max_health_;
	int score_;
	bool is_alive_;
	
	void CreateDynamicBody(b2World *world, float collision_radius);

	// force touch panels
	std::array<Rectangle, 4> force_panels_;
	std::array<abfw::Sprite, 4> debug_tp_;

	void BuildForcePanels();		
	void UseForcePanel(ForceDirection direction);
	void ApplyForce(abfw::Vector3 force_pos, abfw::Vector3 force_direction);

	float force_strength_;
	bool show_panels_;

};

#endif