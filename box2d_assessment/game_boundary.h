#ifndef _GAME_BOUNDARY_H
#define _GAME_BOUNDARY_H

#include <graphics/sprite.h>
#include <graphics/texture.h>
#include <graphics\sprite_renderer.h>

#include "game_object.h"

class GameBoundary : public GameObject {
public:
	GameBoundary(b2World * world, abfw::Vector3 position, float width,float height, UInt32 colour);
	
	~GameBoundary();
	void Render(abfw::SpriteRenderer *sprite_renderer);
protected:
	abfw::Sprite sprite_;

	void CreateStaticBody(b2World *world, float width, float height);

};

#endif 