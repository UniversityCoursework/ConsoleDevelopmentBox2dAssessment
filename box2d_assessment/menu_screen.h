#ifndef _MENU_SCREEN_H
#define _MENU_SCREEN_H

#include <vector>

#include "game_screen.h"
#include "button.h"

class MenuScreen : public GameScreen {
public:
	MenuScreen(abfw::Platform& platform);
	
protected:

	void UpdateMenu(float dt);
	virtual void UseButton(int index) = 0;

	int current_button_;
	std::vector<Button> buttons_;	
	typedef std::vector<Button>::iterator Button_It;
		
	float menu_delay_timer_;
	static const float analog_menu_delay_;

	bool valid_touch_;
};
#endif