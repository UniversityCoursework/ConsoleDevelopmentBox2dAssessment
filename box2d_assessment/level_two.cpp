#include "level_two.h"

#include <system/Platform.h>


void LevelTwo::LoadLevel() {
	currently_loading_ = "Universe 2";
	t_ice_giant_ = LoadTextureFromPNG("bin/ice_giant.png");
	percentage_ += 0.15f;
	t_red_giant_ = LoadTextureFromPNG("bin/red_giant.png");
	percentage_ += 0.15f;
	t_sun_ = LoadTextureFromPNG("bin/sun.png");
	percentage_ += 0.15f;
	t_gas_giant_ = LoadTextureFromPNG("bin/gas_giant.png");
	percentage_ += 0.15f;	
}

void LevelTwo::BuildLevel() {
	spaceship_.Init(world_, abfw::Vector3(1048, 1423, -0.9f), 32, 24, shared_assets_.t_space_ship, t_explosion_);
	spaceship_.set_force_strength(game_settings_->force_strength());	
	// planets
	planet_manager_.AddPlanet(new Planet(world_, abfw::Vector3(2521, 707, -0.82f), 200, 150, t_sun_, 1.f, 1800, t_gravity_well_));

	planet_manager_.AddPlanet(new Planet(world_, abfw::Vector3(2369, 987, -0.84f), 100, 76, t_red_giant_, 1.8f, 180, t_gravity_well_));

	planet_manager_.AddPlanet(new Planet(world_, abfw::Vector3(1808, 1072, -0.86f), 150, 118, t_gas_giant_, 1.8f, 430, t_gravity_well_));

	planet_manager_.AddPlanet(new Planet(world_, abfw::Vector3(1720, 1585, -0.88f), 100, 80, t_ice_giant_, 1.1f, 245, t_gravity_well_));

	// portal
	portal_.Init(world_, abfw::Vector3(2359, 1351, -0.89f), 100, 60, shared_assets_.t_portal);

	// pickups
	//
	pickup_manager_.AddPickUp(new PickUp(world_, abfw::Vector3(1717, 707, -0.89f), 16, 16, shared_assets_.t_pickup, 30));
	pickup_manager_.AddPickUp(new PickUp(world_, abfw::Vector3(1610, 1024, -0.89f), 16, 16, shared_assets_.t_pickup, 30));
	pickup_manager_.AddPickUp(new PickUp(world_, abfw::Vector3(1532, 1284, -0.89f), 16, 16, shared_assets_.t_pickup, 30));
	pickup_manager_.AddPickUp(new PickUp(world_, abfw::Vector3(1439, 1703, -0.89f), 16, 16, shared_assets_.t_pickup, 30));
	pickup_manager_.AddPickUp(new PickUp(world_, abfw::Vector3(1807, 1659, -0.89f), 16, 16, shared_assets_.t_pickup, 30));
	pickup_manager_.AddPickUp(new PickUp(world_, abfw::Vector3(2021, 1216, -0.89f), 16, 16, shared_assets_.t_pickup, 30));
	pickup_manager_.AddPickUp(new PickUp(world_, abfw::Vector3(2303, 409, -0.89f), 16, 16, shared_assets_.t_pickup, 30));
	pickup_manager_.AddPickUp(new PickUp(world_, abfw::Vector3(2675, 804, -0.89f), 16, 16, shared_assets_.t_pickup, 30));
	pickup_manager_.AddPickUp(new PickUp(world_, abfw::Vector3(2469, 1049, -0.89f), 16, 16, shared_assets_.t_pickup, 30));

	//asteroids
	// line of asteroids start point
	float start_x = 3537;
	float start_y = 465;
	AsteroidSpawner* asteroid_spawner;
	for (int i = 0; i < 10; i++){
		asteroid_manager_.AddAsteroid(world_, abfw::Vector3(start_x + (i * 30), start_y, -0.9f), 16.f, 12.f, b2Vec2(-6.f, -112.f), t_asteroid_, t_explosion_);
	}
	start_x = 3537;
	start_y = 1731;
	for (int i = 0; i < 10; i++){
		asteroid_manager_.AddAsteroid(world_, abfw::Vector3(start_x + (i * 30), start_y, -0.9f), 16.f, 12.f, b2Vec2(-128.f, 2.f), t_asteroid_, t_explosion_);
	}
	start_x = 762;
	start_y = 300;
	for (int i = 0; i < 10; i++){
		asteroid_manager_.AddAsteroid(world_, abfw::Vector3(start_x + (i * 30), start_y, -0.9f), 16.f, 12.f, b2Vec2(-6.f, -132.f), t_asteroid_, t_explosion_);
	}
	start_x = 792;
	start_y = 400;
	for (int i = 0; i < 10; i++){
		asteroid_manager_.AddAsteroid(world_, abfw::Vector3(start_x + (i * 30), start_y, -0.9f), 16.f, 12.f, b2Vec2(-6.f, -132.f), t_asteroid_, t_explosion_);
	}
	start_x = 3537;
	start_y = 236;
	for (int i = 0; i < 4; i++){
		asteroid_spawner = new AsteroidSpawner(world_, abfw::Vector3(start_x + (i * 30), start_y, -0.9f), 16.f, 12.f, b2Vec2(-128.f, 2.f), t_asteroid_, t_explosion_);
		asteroid_spawner->set_spawn_delay(4);
		asteroid_spawner->force_spawn();
		asteroid_manager_.AddSpawner(asteroid_spawner);
	}
	start_x = 537;
	start_y = 236;
	for (int i = 0; i < 6; i++){
		asteroid_spawner = new AsteroidSpawner(world_, abfw::Vector3(start_x + (i * 30), start_y, -0.9f), 16.f, 12.f, b2Vec2(128.f, 2.f), t_asteroid_, t_explosion_);
		asteroid_spawner->set_spawn_delay(4);
		asteroid_spawner->force_spawn();
		asteroid_manager_.AddSpawner(asteroid_spawner);
	}
}

void LevelTwo::CleanUpLevel() {
	delete t_sun_;
	t_sun_ = nullptr;
	delete t_red_giant_;
	t_red_giant_ = nullptr;
	delete t_gas_giant_;
	t_gas_giant_ = nullptr;
	delete t_ice_giant_;
	t_ice_giant_ = nullptr;
}