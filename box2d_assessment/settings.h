#ifndef _SETTINGS_H
#define _SETTINGS_H

#include <maths\math_utils.h>

const float default_vol = 1.0f;
const float max_vol = 1.0f;
const float min_vol = 0.0f;

class Settings{
public:
	Settings(){
		sfx_volume_ = default_vol;
		music_volume_ = default_vol;
		master_volume_ = default_vol;
	}

	void set_sfx_volume(float val){
		sfx_volume_ = abfw::clamp(val, min_vol, max_vol);
	}
	void set_music_volume(float val){
		music_volume_ = abfw::clamp(val, min_vol, max_vol);
	}
	void set_master_volume(float val){
		master_volume_ = abfw::clamp(val, min_vol, max_vol);
	}

	float sfx_volume(){ return sfx_volume_; }
	float music_volume(){ return music_volume_; }
	float master_volume(){ return master_volume_; }

protected:
	float sfx_volume_;
	float music_volume_;
	float master_volume_;
};

#endif