#ifndef _PICKUP_H
#define _PICKUP_H

#include "game_object.h"

#include <graphics/texture.h>
#include <maths/vector3.h>
#include <graphics/sprite.h>
#include <graphics\sprite_renderer.h>

#include "game_object.h"
#include "animated_sprite.h"

class PickUp : public GameObject {
public:
	~PickUp();
	PickUp(b2World* world, abfw::Vector3 positon, float texture_radius, float collision_radius, abfw::Texture* base_texture, int points);
	
	int GetPoints();	

	void Render(abfw::SpriteRenderer *sprite_renderer);
protected:
	void CreateStaticBody(b2World *world, float collision_radius);	
	abfw::Sprite sprite_;
	int points_;
};

#endif
