#include "main_menu.h"

#include <input\touch_input_manager.h>
#include <system/platform.h>

#include "screen_manager.h"

#include "option_screen.h"
#include "loading_screen.h"
#include "tutorial_screen.h"

#include "level_one.h"
#include "level_two.h"

enum ButtonName{
	kLevelOne = 0,
	kLevelTwo,
	kTutorial,
	kOptions
};

const float button_width = 280;
const float button_height = 60;
const float button_offset = 62;
const float button_start = 186;

const float title_offset = 120;

const float bg_height_offset = 200;

const UInt32 bg_colour = 0x805D1372;
const UInt32 button_bg_colour = 0xFFCFA037;
const UInt32 button_highlight_colour = 0x7DFFFFFF;

// allows multi threading of the load on vita
#ifdef _PLATFORM_VITA
static Int32 LoadMainMenu(SceSize args, void *argc) {
	MainMenu* game = *(MainMenu**)argc;
	game->Load();
	return 0;
}
#endif

MainMenu::MainMenu(abfw::Platform& platform)
: MenuScreen(platform) {

}

void MainMenu::Init() {	
#ifdef _PLATFORM_VITA	
	LoadStartThread(LoadMainMenu);	
#else	
	Load();
#endif
}

void MainMenu::Load(){
	percentage_ = 0.0f;
	banner_.set_position(platform_.width() / 2, platform_.height() / 2, -0.9f);
	banner_.set_width(platform_.width());
	banner_.set_height(platform_.height() - bg_height_offset);
	banner_.set_colour(bg_colour);
		
	currently_loading_ = "Planets";
	shared_assets_.t_portal = LoadTextureFromPNG("bin/exo_2.png");
	
	percentage_ += 0.1f;
	currently_loading_ = "SpaceShips";
	shared_assets_.t_space_ship = LoadTextureFromPNG("bin/strange_thingy.png");
	percentage_ += 0.1f;

	currently_loading_ = "PickUps";
	shared_assets_.t_pickup = LoadTextureFromPNG("bin/pickup.png");
	percentage_ += 0.1f;

	currently_loading_ = "All of time and Space";
	shared_assets_.t_background = LoadTextureFromPNG("bin/space.png");
	percentage_ += 0.4f;
	
	background_.set_position(platform_.width() / 2, platform_.height() / 2, 0);
	background_.set_width(platform_.width());
	background_.set_height(platform_.height());	
	background_.set_texture(shared_assets_.t_background);
	

	current_button_ = 0;
	// kLevelOne
	buttons_.push_back(Button(abfw::Vector3(platform_.width() / 2, button_start , -0.9f), button_width, button_height, button_bg_colour, button_highlight_colour, "Level 1"));
	// kLevelTwo
	buttons_.push_back(Button(abfw::Vector3(platform_.width() / 2, button_start + button_offset, -0.9f), button_width, button_height, button_bg_colour, button_highlight_colour, "Level 2"));
	// kTutorial
	buttons_.push_back(Button(abfw::Vector3(platform_.width() / 2, button_start + 2* button_offset, -0.9f), button_width, button_height, button_bg_colour, button_highlight_colour, "Tutorial"));
	// kOptions
	buttons_.push_back(Button(abfw::Vector3(platform_.width() / 2, button_start + 3 * button_offset, -0.9f), button_width, button_height, button_bg_colour, button_highlight_colour, "Options"));
		
	valid_touch_ = false;
	percentage_ += 0.1f;
#ifdef _PLATFORM_VITA
	currently_loading_ = "All the music!!!";
	screen_manager_->GetAudioManager()->LoadMusic("hero_immortal.wav", platform_);
	screen_manager_->GetAudioManager()->PlayMusic();
	percentage_ += 0.3f;
#endif
	loading_ = false;
}

void MainMenu::CleanUp(){
	// ensure everything is deleted	
}

bool MainMenu::IsLoading(float &percentage, std::string &text){
	percentage = percentage_;
	text = currently_loading_;
	return loading_;
}

void MainMenu::Update(float dt){
	switch(state_) {
		case kActive:{	
				UpdateMenu(dt);
				break;
			}
		case kOverlayed: {	
			#ifdef _PLATFORM_VITA
			if (!loading_ &&thread_running_){
				LoadStopThread();				
			}
			#endif
				break;
			}
		case kHidden:
			// any background animations, updates etc.
			break;
		default:
			break;
	}
}

void MainMenu::UseButton(int index) {
	switch(index) {
	case kLevelOne: {
				screen_manager_->PushScreen(new LevelOne(platform_, shared_assets_));
				Pause(true);				
				break;				
			}
	case kLevelTwo: {
				screen_manager_->PushScreen(new LevelTwo(platform_, shared_assets_));
				Pause(true);
				break;
			}
	case kTutorial:{	
				screen_manager_->PushScreen(new TutorialScreen(platform_,shared_assets_));
				Pause(true);
				break;
			}
	case kOptions:{				
				screen_manager_->PushScreen(new OptionScreen(platform_));
				Pause(true);				
				break;
			}
		default:
			break;
	}
}

void MainMenu::Render(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font){	
	switch (state_)	{
	case kActive:		
		sprite_renderer->DrawSprite(background_);
		sprite_renderer->DrawSprite(banner_);
		
		// render menu buttons
		font->RenderText(sprite_renderer, abfw::Vector3(platform_.width() / 2, title_offset, -0.9f), 1.0f, 0xffffffff, abfw::TJ_CENTRE, "Main Menu");
		for (Button_It button = buttons_.begin(); button != buttons_.end(); button++){
			button->Render(sprite_renderer, font);
		}		
		break;
	case kOverlayed:
		sprite_renderer->DrawSprite(background_);
		break;
	default:
		break;
	}
}
