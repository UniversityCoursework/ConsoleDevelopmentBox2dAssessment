#ifndef _LOSER_SCREEN_H
#define _LOSER_SCREEN_H

#include "menu_screen.h"
#include "button.h"

class LoserScreen : public MenuScreen {

public:
	LoserScreen(abfw::Platform& platform);
	void Init() override;
	void CleanUp() override;

	void Update(float dt) override;
	void UseButton(int index) override;
	void Render(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font) override;

protected:
	abfw::Sprite background_;
};
#endif

