#ifndef _TUTORIAL_SCREEN_H
#define _TUTORIAL_SCREEN_H

#include "menu_screen.h"
#include "button.h"
#include "shared_assets.h"

class TutorialScreen : public MenuScreen {

public:
	TutorialScreen(abfw::Platform& platform,SharedAssets &shared_assets);
	void Init() override;
	void CleanUp() override;

	void Update(float dt) override;
	void UseButton(int index) override;
	void Render(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font) override;

protected:
	abfw::Sprite background_;
	abfw::Sprite tut_image_;
	
	typedef std::vector<std::string>::iterator String_It;
	std::vector<std::string> *description_;

	std::vector<std::string> destination_description_;
	std::vector<std::string> survivor_description_;
	std::vector<std::string> player_description_;

	SharedAssets &shared_assets_;

};
#endif

