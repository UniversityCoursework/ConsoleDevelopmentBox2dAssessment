#ifndef _WIN_SCREEN_H
#define _WIN_SCREEN_H

#include "menu_screen.h"
#include "button.h"

class WinScreen : public MenuScreen {

public:
	WinScreen(abfw::Platform& platform,int score);
	void Init() override;
	void CleanUp() override;
	
	void Update(float dt) override;
	void UseButton(int index) override;
	void Render(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font) override;

protected:
	abfw::Sprite background_;
	
	int score_;
};
#endif // !_WIN_SCREEN_H

