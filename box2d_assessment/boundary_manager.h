#ifndef _BOUNDARY_MANAGER_H
#define _BOUNDARY_MANAGER_H

#include <vector>
#include "game_boundary.h"

class GameBoundaryManager {

public:
	void CleanUp();

	void AddGameEdge(GameBoundary *edge);
	
	void Render(abfw::SpriteRenderer *renderer);		
private:
	std::vector<GameBoundary* > game_boundaries_;
	typedef std::vector<GameBoundary*>::iterator GameBoundary_it;

};

#endif
