#include "planet.h"

#include "Box2D\Box2D.h"
#include "box2d_helpers.h"

const float density_ = 10.0f;
const float friction_ = 1.0f;
const float restitution_ = 0.0f;

const float gravity_well_z_offset = 0.01f;

Planet::Planet(b2World * world, abfw::Vector3 position, float radius, float collision_radius, abfw::Texture * planet, float gravity, float gravity_radius, abfw::Texture * bg_radius) {
	radius *= 2;
	gravity_radius *= 2;
	planet_.set_width(radius);
	planet_.set_height(radius);
	planet_.set_position(position);
	planet_.set_texture(planet);	

	gravity_ = gravity;
	gravity_radius_ = ABFW_BOX2D_SIZE(gravity_radius/2);
	
	planet_bg.set_width(gravity_radius);
	planet_bg.set_height(gravity_radius);
	planet_bg.set_position(position.x, position.y, position.z + gravity_well_z_offset);
	planet_bg.set_texture(bg_radius);

	CreateStaticBody(world, collision_radius);
	set_type(kPlanet);	
}

Planet::~Planet() {
	body_->GetWorld()->DestroyBody(body_);
}

void Planet::Update(float dt) {
	
}

void Planet::Render(abfw::SpriteRenderer * sprite_renderer) {
	
	sprite_renderer->DrawSprite(planet_bg);
	sprite_renderer->DrawSprite(planet_);
}

void Planet::CreateStaticBody(b2World * world, float collision_radius) {
	b2BodyDef body_def;
	body_def.type = b2_staticBody;
	body_def.position = b2Vec2(ABFW_BOX2D_POS_X(planet_.position().x), ABFW_BOX2D_POS_Y(planet_.position().y));
	body_ = world->CreateBody(&body_def);
	
	b2CircleShape circle;
	circle.m_p.SetZero();
	circle.m_radius = ABFW_BOX2D_SIZE(collision_radius);
	
	b2FixtureDef fixtureDef;	
	fixtureDef.shape = &circle;
	fixtureDef.density = density_;
	fixtureDef.friction = friction_;
	fixtureDef.restitution = restitution_;	
	fixtureDef.filter.categoryBits = kPlanet;
	fixtureDef.filter.maskBits = kAsteroid | kPlayer;
	
	body_->CreateFixture(&fixtureDef);
	
	body_->SetUserData(this);
}
