#ifndef _GAME_OBJECT_H
#define _GAME_OBJECT_H

#include <Box2D/Box2D.h>

enum ObjectType {
	kPlanet = 0x0001,
	kAsteroid = 0x0002,
	kPlayer = 0x0004,
	kPortal = 0x0008,
	kPickUp = 0x0010,
	kBoundary
};

class GameObject {
public:	
	GameObject() :body_(nullptr), needs_deleted_(false){}
	virtual ~GameObject() { };
	virtual void Update(float dt){};

	void set_needs_deleted(bool val) { needs_deleted_ = val; }
	bool const needs_deleted() const { return needs_deleted_; }
	
	ObjectType type() { return type_; }
	void set_type(ObjectType type) { type_ = type; }

	b2Body *body() { return body_; }
		
protected:
	void DestroyBody() {
		if(body_ != nullptr) {
			body_->GetWorld()->DestroyBody(body_);
			body_ = nullptr;
		}
	}
	b2Body* body_;
	bool needs_deleted_;	
	ObjectType type_;
};

#endif