#ifndef _SCREEN_MANAGER_H
#define _SCREEN_MANAGER_H

#include <vector>
#include <graphics/sprite.h>
#include <graphics/font.h>
#include <input/sony_controller_input_manager.h>
#include <input/touch_input_manager.h>


#ifdef _PLATFORM_VITA
#include <audio/vita/audio_manager_vita.h>
#endif

#include "game_screen.h"
#include "settings.h"

class ScreenManager {
public:
#ifdef _PLATFORM_VITA
	void Init(abfw::SonyControllerInputManager* controller_manager, 
		abfw::TouchInputManager* touch_manager,Settings *settings, 
		abfw::AudioManagerVita *audio_manager);

	abfw::AudioManagerVita* GetAudioManager() const { return audio_manager_; }
	
#else
	void Init(abfw::SonyControllerInputManager* controller_manager, abfw::TouchInputManager* touch_manager, Settings *settings);
#endif	
	void CleanUp();	
	void PushScreen(GameScreen* screen);	
	void PopScreen(int number_of_screens);	
	void PopScreen();

	void Update(float dt);
	void Render(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font_);

	abfw::SonyControllerInputManager* GetControllerManager() const { return controller_manager_; }
	abfw::TouchInputManager* GetTouchManager() const { return touch_manager_; }
	Settings* GetSettings(){ return settings_; }
	std::vector<GameScreen* >::iterator GetIterTopScreen() {		
		return screens_.end()-1;		
	}
	int NumberOfScreens() { return screens_.size(); }

protected:
	std::vector<GameScreen* > screens_;

	typedef std::vector<GameScreen*>::iterator Screen_It;
	// pointers to input managers shared between all screens.
	abfw::SonyControllerInputManager* controller_manager_;
	abfw::TouchInputManager* touch_manager_;

	Settings *settings_;
	
#ifdef _PLATFORM_VITA
	abfw::AudioManagerVita *audio_manager_;	
#endif
	
};

#endif