#ifndef _PAUSE_SCREEN_H
#define _PAUSE_SCREEN_H

#include <graphics/sprite.h>

#include "menu_screen.h"
#include "button.h"
#include "shared_assets.h"

class PauseScreen : public MenuScreen {
public:
	PauseScreen(abfw::Platform& platform, SharedAssets &shared_assets);
	void Init() override;
	void CleanUp() override;

	void Update(float dt) override;
	void Render(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font_) override;

protected:	
	void UseButton(int index) override;
	abfw::Sprite background_;

	SharedAssets &shared_assets_;	
};
#endif
