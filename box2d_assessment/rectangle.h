#ifndef _RECTANGLE_H
#define _RECTANGLE_H

#include <maths\vector2.h>

///<summary> Rectangle class used for simplified collision detection
class Rectangle {
public:
	Rectangle();
	///<summary> Remember x,y should be moved to the centre. PSVita sprite x/y is centered. </summary>
	Rectangle(float x, float y, float half_width, float half_height);
	~Rectangle() {};
	///<summary> Returns true if the point is within the Rectangle. </summary>
	bool ContainsPoint(const abfw::Vector2 &point);
	///<summary> Remember x,y should be the centre of the rectangle. </summary>
	inline void set_position(const float x, const float y) { x_ = x; y_ = y; }
	inline void set_half_width(const float half_width) { half_width_ = half_width; }
	inline void set_half_height(const float half_height) { half_height_ = half_height; }

	inline const float x() const { return x_; };
	inline const float y() const { return y_; };
	inline const float half_width() const { return half_width_; };
	inline const float half_height() const { return half_height_; };

protected:
	float x_, y_, half_width_, half_height_;
};

#endif
