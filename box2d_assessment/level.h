#ifndef _LEVEL_H
#define _LEVEL_H

#include <graphics/sprite.h>
#include <Box2D/Box2D.h>

#include "game_screen.h"

#include "asteroid_manager.h"
#include "planet_manager.h"
#include "player.h"
#include "portal.h"
#include "pick_up_manager.h"

#include "main_contact_listener.h"
#include "game_settings.h"

#include "boundary_manager.h"
#include "shared_assets.h"

class Level : public GameScreen {
public:
	Level(abfw::Platform& platform, SharedAssets &shared_assets) :
		GameScreen(platform), shared_assets_(shared_assets){
	}

	void Init() override;
	void Load();
	void CleanUp() override;

	bool IsLoading(float &percentage, std::string &text) override;

	void Pause(bool overlay) override;
	void Resume() override;
	void Update(float dt) override;
	void Render(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font_) override;

protected:
	// Load any suplemental assets, or audio
	virtual void LoadLevel(){};
	// Build the scene, portal ship initialization etc.
	virtual void BuildLevel(){};

	// Update any looping audio after resume
	virtual void ResumeAudio(){};
	// Stop any looping audio when game pauses.
	virtual void StopAudio(){};

	// Clean up any suplemental assets, or audio
	virtual void CleanUpLevel(){};

	void ApplyWellForce(Planet* planet, GameObject* gameobject);
	void ApplyGravityWellForces();

	abfw::Sprite background_;

	GameSettings* game_settings_;

	b2World* world_;
	MainContactListener contact_listener_;

	// dirty managers, just update render and handle safe deletion of objects.
	AsteroidManager asteroid_manager_;
	GameBoundaryManager boundary_manager_;
	PlanetManager planet_manager_;
	PickUpManager pickup_manager_;

	Player spaceship_;
	Portal portal_;

	abfw::Texture *t_gravity_well_;
	abfw::Texture *t_sun_;
	abfw::Texture *t_asteroid_;
	abfw::Texture *t_explosion_;

	SharedAssets &shared_assets_;

	std::string currently_loading_;

#ifdef _PLATFORM_VITA
	Int32 explosion_sfx_id;
	Int32 pick_up_sfx_id_;
	Int32 pick_up_voice_;
#endif
	bool follow_cam;

};
#endif
