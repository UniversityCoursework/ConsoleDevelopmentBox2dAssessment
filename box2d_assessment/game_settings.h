#ifndef _GAME_SETTINGS_H
#define _GAME_SETTINGS_H

#include "settings.h"

const float default_force = 4.0f;
const float default_sfx_vol = 0.6f;

const float max_force = 100.f;
const float min_force = 1.0f;

class GameSettings : public Settings{
public:
	GameSettings() :Settings(){
		force_strength_ = default_force;
		sfx_volume_ = default_sfx_vol;
	}
	void set_force_strength(float val){
		force_strength_ = abfw::clamp(val, min_force, max_force);
	}
	float force_strength(){ return force_strength_; }

	void set_last_score(int val){ score_ = val; }
	int last_score(){ return score_; }
protected:
	float force_strength_;
	int score_;

};
#endif