#ifndef _BUTTON_H
#define _BUTTON_H

#include <graphics/sprite.h>
#include <graphics/sprite_renderer.h>
#include <graphics/font.h>
#include <string>

// very basic button, 2 layers simple 2-d point intersetion test
class Button {
public:
	Button(){};
	Button(abfw::Vector3 position, float width, float height, abfw::Texture* bg, abfw::Texture* highlight, std::string text);
	Button(abfw::Vector3 position, float width, float height, UInt32 bg_color, UInt32 highlight_colour, std::string text);
	bool ContainsPoint(const abfw::Vector3 & point);
	bool is_active() { return is_active_; }
	void set_is_active(bool val){ is_active_ = val; }
	///<summary> Handles rendering the seperate layers of the button. </summary>
	void Render(abfw::SpriteRenderer* renderer, abfw::Font *font);	

protected:	
	bool is_active_;
	abfw::Vector3 text_position_;
	std::string text_;
	abfw::Sprite background_;
	abfw::Sprite highlight_;

private:
	float x_, y_, half_width_,half_height_;
	
};
#endif