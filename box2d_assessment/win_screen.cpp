#include "win_screen.h"

#include <input\touch_input_manager.h>
#include <system/platform.h>

#include "screen_manager.h"

const float button_width = 280;
const float button_height = 60;
const float button_offset = 132;

const float title_offset = 130;

const float controls_start = 220;

const float bg_height_offset = 200;
const UInt32 bg_colour = 0x805D1372;


WinScreen::WinScreen(abfw::Platform & platform, int score)
	:MenuScreen(platform) {
	score_ = score;
}

void WinScreen::Init() {
	background_.set_position(platform_.width() / 2, platform_.height() / 2, -0.9f);
	background_.set_width(platform_.width());
	background_.set_height(platform_.height() - bg_height_offset);
	background_.set_colour(bg_colour);
	current_button_ = -1;
	// continue
	buttons_.push_back(Button(abfw::Vector3(platform_.width() / 2, platform_.height() - button_offset, -0.9f), button_width, button_height, 0xFFCFA037, 0x7DFFFFFF, "Continue"));
}

void WinScreen::CleanUp() {
}

void WinScreen::Update(float dt) {
	switch(state_) {
		case kActive: {
				UpdateMenu(dt);
				break;
			}
		case kOverlayed: {
				// any background animations, updates etc.
				break;
			}
		default:
			break;
	}
}

void WinScreen::UseButton(int index) {
	if(index == 0) {
		screen_manager_->PopScreen(2);
	}
}

void WinScreen::Render(abfw::SpriteRenderer * sprite_renderer, abfw::Font * font) {
	switch(state_) {
		case kActive:
			sprite_renderer->DrawSprite(background_);
			font->RenderText(sprite_renderer, abfw::Vector3(platform_.width() / 2, title_offset, -0.9f), 1.0f, 0xffffffff, abfw::TJ_CENTRE, "You Win!!!");
			font->RenderText(sprite_renderer, abfw::Vector3(platform_.width() / 2, controls_start, -0.9f), 1.0f, 0xffffffff, abfw::TJ_CENTRE, "%0d people saved, your a Hero.", score_);
			for(Button_It button = buttons_.begin(); button != buttons_.end(); button++) {
				button->Render(sprite_renderer, font);
			}			
			break;
		case kOverlayed:
			break;
		default:
			break;
	}
}
