#include "pick_up.h"

#include "box2d_helpers.h"


PickUp::~PickUp(){
	DestroyBody();
}
PickUp::PickUp(b2World* world, abfw::Vector3 position, float texture_radius, float collision_radius, abfw::Texture* base_texture,int points){
	texture_radius *= 2;
	sprite_.set_width(texture_radius);
	sprite_.set_height(texture_radius);
	sprite_.set_position(position);
	sprite_.set_texture(base_texture);
	CreateStaticBody(world, collision_radius);
	set_type(kPickUp);
	needs_deleted_ = false;
	points_ = points;
}
int PickUp::GetPoints(){
	if (!needs_deleted_){
		needs_deleted_ = true;
		return  points_;
	}
	return 0;
}

void PickUp::CreateStaticBody(b2World *world, float collision_radius){
	b2BodyDef body_def;
	body_def.type = b2_staticBody;
	body_def.position = b2Vec2(ABFW_BOX2D_POS_X(sprite_.position().x), ABFW_BOX2D_POS_Y(sprite_.position().y));
	body_ = world->CreateBody(&body_def);

	b2CircleShape circle;
	circle.m_p.SetZero();
	circle.m_radius = ABFW_BOX2D_SIZE(collision_radius);

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circle;	
	fixtureDef.isSensor = true;
	fixtureDef.filter.categoryBits = kPickUp;
	fixtureDef.filter.maskBits = kPlayer;

	body_->CreateFixture(&fixtureDef);

	body_->SetUserData(this);
}

void PickUp::Render(abfw::SpriteRenderer *sprite_renderer){	
	sprite_renderer->DrawSprite(sprite_);	

}