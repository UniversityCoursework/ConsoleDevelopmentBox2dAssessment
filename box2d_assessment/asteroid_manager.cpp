#include "asteroid_manager.h"
void AsteroidManager::AddAsteroid(b2World* world, abfw::Vector3 position, float radius, float collision_radius, b2Vec2 direction, abfw::Texture* texture, abfw::Texture * explosion_texture){
	asteroids_.push_back(new Asteroid(world, position, radius, collision_radius, direction, texture, explosion_texture));
}
void AsteroidManager::CleanUp() {
	// Spawners
	for (Spawner_It spawner = spawners_.begin(); spawner != spawners_.end(); spawner++) {
		delete(*spawner);
		(*spawner) = nullptr;		
	}
	spawners_.clear();

	// Asteroids
	for (Asteroid_It asteroid = asteroids_.begin(); asteroid != asteroids_.end(); ++asteroid) {
		delete(*asteroid);
		(*asteroid) = nullptr;
	}
	asteroids_.clear();
}

void AsteroidManager::Update(float dt) {
	for (Spawner_It spawner = spawners_.begin(); spawner != spawners_.end(); ++spawner) {
		(*spawner)->Update(dt);
	}
	for (Asteroid_It asteroid = asteroids_.begin(); asteroid != asteroids_.end();) {
		if((*asteroid)->needs_deleted()) {
			delete (*asteroid);
			(*asteroid) = nullptr;
			asteroid = asteroids_.erase(asteroid);
		} else {
			(*asteroid)->Update(dt);
			++asteroid;
		}
	}
}
void AsteroidManager::Render(abfw::SpriteRenderer *renderer) {
	for (Asteroid_It asteroid = asteroids_.begin(); asteroid != asteroids_.end(); ++asteroid) {
		if(!(*asteroid)->needs_deleted()) {
			(*asteroid)->Render(renderer);
		}
	}
}

std::vector<Asteroid*> &AsteroidManager::GetAsteroids() {
	return asteroids_;
}

void AsteroidManager::AddSpawner(AsteroidSpawner * spawner) {
	spawner->SetAsteroids(&asteroids_);
	spawners_.push_back(spawner);
}
