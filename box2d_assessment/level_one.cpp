#include "level_one.h"

#include <system/Platform.h>


void LevelOne::LoadLevel() {
	currently_loading_ = "Universe 1";	
	t_sun_ = LoadTextureFromPNG("bin/sun.png");
	percentage_ += 0.15f;
	t_gas_giant_ = LoadTextureFromPNG("bin/gas_giant.png");
	percentage_ += 0.15f;
}

void LevelOne::BuildLevel() {	
	// planets
	planet_manager_.AddPlanet(new Planet(world_, abfw::Vector3(2521, 707, -0.82f), 200, 150, t_sun_, 1.2f, 2000, t_gravity_well_));
	
	planet_manager_.AddPlanet(new Planet(world_, abfw::Vector3(1828, 972, -0.86f), 150, 120, t_gas_giant_, 1.8f, 430, t_gravity_well_));
	
	// portal
	portal_.Init(world_, abfw::Vector3(2359, 1351, -0.89f), 100, 60, shared_assets_.t_portal);

	// pickups		
	pickup_manager_.AddPickUp(new PickUp(world_, abfw::Vector3(1610, 1024, -0.89f), 16, 16, shared_assets_.t_pickup, 30));

	//asteroids
	// line of asteroids start point
	float start_x = 3537;	
	float start_y = 465;	
	AsteroidSpawner* asteroid_spawner;
	for (int i = 0; i < 10; i++){
		asteroid_manager_.AddAsteroid(world_, abfw::Vector3(start_x + (i * 30), start_y, -0.9f), 16.f, 12.f, b2Vec2(-6.f, -112.f), t_asteroid_, t_explosion_);		
	}
	start_x = 3537;
	start_y = 1731;
	for (int i = 0; i < 10; i++){
		asteroid_manager_.AddAsteroid(world_, abfw::Vector3(start_x + (i * 30), start_y, -0.9f), 16.f, 12.f, b2Vec2(-128.f, 2.f), t_asteroid_, t_explosion_);
	}
	start_x = 762;
	start_y = 300;
	for (int i = 0; i < 10; i++){
		asteroid_manager_.AddAsteroid(world_, abfw::Vector3(start_x + (i * 30), start_y, -0.9f), 16.f, 12.f, b2Vec2(-6.f, -132.f), t_asteroid_, t_explosion_);
	}
	start_x = 792;
	start_y = 400;
	for (int i = 0; i < 10; i++){
		asteroid_manager_.AddAsteroid(world_, abfw::Vector3(start_x + (i * 30), start_y, -0.9f), 16.f, 12.f, b2Vec2(-6.f, -132.f), t_asteroid_, t_explosion_);
	}
	start_x = 3537;
	start_y = 236;
	for (int i = 0; i < 4; i++){
		asteroid_spawner = new AsteroidSpawner(world_, abfw::Vector3(start_x + (i * 30), start_y, -0.9f), 16.f, 12.f, b2Vec2(-128.f, 2.f), t_asteroid_, t_explosion_);
		asteroid_spawner->set_spawn_delay(4);
		asteroid_spawner->force_spawn();
		asteroid_manager_.AddSpawner(asteroid_spawner);
	}
	start_x = 537;
	start_y = 236;
	for (int i = 0; i < 6; i++){
		asteroid_spawner = new AsteroidSpawner(world_, abfw::Vector3(start_x + (i * 30), start_y, -0.9f), 16.f, 12.f, b2Vec2(128.f, 2.f), t_asteroid_, t_explosion_);
		asteroid_spawner->set_spawn_delay(4);
		asteroid_spawner->force_spawn();
		asteroid_manager_.AddSpawner(asteroid_spawner);
	}

	// spaceship
	spaceship_.Init(world_, abfw::Vector3(1048, 1423, -0.9f), 32, 24, shared_assets_.t_space_ship, t_explosion_);
	spaceship_.set_force_strength(game_settings_->force_strength());
	spaceship_.set_show_panels(true);
}

void LevelOne::CleanUpLevel() {
	delete t_sun_;
	t_sun_ = nullptr;	
	delete t_gas_giant_;
	t_gas_giant_ = nullptr;

}