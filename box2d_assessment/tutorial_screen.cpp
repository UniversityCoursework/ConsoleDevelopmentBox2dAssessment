#include "tutorial_screen.h"

#include <input\touch_input_manager.h>
#include <system/platform.h>

#include "screen_manager.h"

const float button_width = 180;
const float button_height = 60;
const float button_offset_x = 86;
const float button_offset_y = 62;

const float continue_offset = 82;

const float title_offset = 60;

const float text_pos_x = 200;
const float text_pos_y = 120;
const float text_line_spacing = 24;

const float controls_start = 180;

const float bg_height_offset = 100;

const UInt32 bg_colour = 0x805D1372;
const UInt32 button_bg_colour = 0xFFCFA037;
const UInt32 button_highlight_colour = 0x7DFFFFFF;

const float img_size = 200;
const float img_x_offset = 150;

enum ButtonName{	
	kDesination = 0,
	kOrbs,
	kPlayer,
	kContinue,
};


TutorialScreen::TutorialScreen(abfw::Platform &platform, SharedAssets &shared_assets)
:MenuScreen(platform),shared_assets_(shared_assets) {	
}

void TutorialScreen::Init() {
	
	background_.set_position(platform_.width() / 2, platform_.height() / 2, -0.9f);
	background_.set_width(platform_.width());
	background_.set_height(platform_.height() - bg_height_offset);
	background_.set_colour(bg_colour);

	tut_image_.set_position(platform_.width() - img_x_offset, controls_start, -0.9f);
	tut_image_.set_width(img_size);
	tut_image_.set_height(img_size);

	current_button_ = -1;

	
	// kDesination
	buttons_.push_back(Button(abfw::Vector3(button_offset_x, controls_start, -0.9f), button_width, button_height, button_bg_colour, button_highlight_colour, "Target"));
	// kOrbs
	buttons_.push_back(Button(abfw::Vector3(button_offset_x, controls_start + button_offset_y, -0.9f), button_width, button_height, button_bg_colour, button_highlight_colour, "Pick Ups"));
	// kPlayer
	buttons_.push_back(Button(abfw::Vector3(button_offset_x, controls_start + 2 * button_offset_y, -0.9f), button_width, button_height, button_bg_colour, button_highlight_colour, "Ooze Asteroid"));
	// continue
	buttons_.push_back(Button(abfw::Vector3(platform_.width() / 2, platform_.height() - continue_offset, -0.9f), button_width, button_height, 0xFFCFA037, 0x7DFFFFFF, "Return"));
	
	// could wrap this up into a text box class with size,text and spacing font etc... but ran out of time sadly.
	destination_description_.push_back("You are looking for fertile planets to crash");
	destination_description_.push_back("your asteroid into. To add to the primordial ");
	destination_description_.push_back("soup of the planet.");
	destination_description_.push_back("To create your civilisation of awesome slime.");

	survivor_description_.push_back("There is primordial ooze throughout the");
	survivor_description_.push_back("galaxy. Collect these to add to your asteroids.");
	survivor_description_.push_back("This will increase the potential of your");
	survivor_description_.push_back("Civilisation of slime.");

	player_description_.push_back("The Players asteroid is controled through use");
	player_description_.push_back("of the front touch screen. Each edge effects ");
	player_description_.push_back("the direction of the asteroid oozey thingy.");	
	player_description_.push_back("");	
	player_description_.push_back("Each edge will apply a force to the asteroid ");
	player_description_.push_back("from that direction.");
	player_description_.push_back("");
	player_description_.push_back("Also during the game, press start to bring");
	player_description_.push_back("up the pause menu,to see any of this again.");
	
	

	description_ = &destination_description_;
	tut_image_.set_texture(shared_assets_.t_portal);
}

void TutorialScreen::CleanUp() {
}

void TutorialScreen::Update(float dt) {
	switch (state_) {
	case kActive: {
		UpdateMenu(dt);
		break;
	}
	case kOverlayed: {
		// any background animations, updates etc.
		break;
	}
	default:
		break;
	}
}

void TutorialScreen::UseButton(int index) {
	switch ((ButtonName)(index)) {
		case kContinue:
			screen_manager_->PopScreen();
			break;
		case kDesination:
			tut_image_.set_texture(shared_assets_.t_portal);
			description_ = &destination_description_;
			break;
		case kOrbs:
			tut_image_.set_texture(shared_assets_.t_pickup);
			description_ = &survivor_description_;
			break;
		case kPlayer:
			tut_image_.set_texture(shared_assets_.t_space_ship);
			description_ = &player_description_;
			break;
	default:
		break;
	}
}

void TutorialScreen::Render(abfw::SpriteRenderer * sprite_renderer, abfw::Font * font) {
	switch (state_) {
		case kActive: {
			sprite_renderer->DrawSprite(background_);
			sprite_renderer->DrawSprite(tut_image_);
			font->RenderText(sprite_renderer, abfw::Vector3(platform_.width() / 2, title_offset, -0.9f), 1.0f, 0xffffffff, abfw::TJ_CENTRE, "Tutorial");
			for(Button_It button = buttons_.begin(); button != buttons_.end(); button++) {
				button->Render(sprite_renderer, font);
			}
			float pos_y = text_pos_y;
			for(String_It line = description_->begin(); line != description_->end(); line++) {
				font->RenderText(sprite_renderer, abfw::Vector3(text_pos_x, pos_y, -0.9f), 1.0f, 0xffffffff, abfw::TJ_LEFT, line->c_str());
				pos_y += text_line_spacing;
			}

			break;
		}
		case kOverlayed:
			break;
		default:
			break;
	}
}
