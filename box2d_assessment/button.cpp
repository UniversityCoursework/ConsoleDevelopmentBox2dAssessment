#include "button.h"

Button::Button(abfw::Vector3 position,float width,float height, UInt32 bg_color, UInt32 highlight_colour, std::string text){
	background_.set_position(position);
	background_.set_width(width);
	background_.set_height(height);
	background_.set_colour(bg_color);

	highlight_.set_position(position.x, position.y, position.z - 0.01f );
	highlight_.set_width(width);
	highlight_.set_height(height);
	highlight_.set_colour(highlight_colour);

	text_position_ = abfw::Vector3(position.x,position.y,position.z - 0.02f);
	text_ = text;
	is_active_ = false;
	x_ = position.x;
	y_ = position.y;
	half_width_ = width / 2;
	half_height_ = height / 2;
}

Button::Button(abfw::Vector3 position, float width, float height, abfw::Texture* background, abfw::Texture* highlight, std::string text)
	:Button(position,width,height,0xFFFFFFFF, 0xFFFFFFFF, text){
	background_.set_texture(background);
	highlight_.set_texture(highlight);	
}

bool Button::ContainsPoint(const abfw::Vector3 &point) {
	if((x_ + half_width_) < (point.x)) return false;
	if((x_ - half_width_) > (point.x)) return false;
	if((y_ + half_height_) < (point.y)) return false;
	if((y_ - half_height_) > (point.y)) return false;
	return true;
}

void Button::Render(abfw::SpriteRenderer* renderer, abfw::Font *font){
	renderer->DrawSprite(background_);	
	if (is_active_){
		renderer->DrawSprite(highlight_);
	}	
	font->RenderText(renderer, text_position_, 1.0f, 0xffffffff, abfw::TJ_VerticalCentre, text_.c_str());
}