#ifndef _SPRITE_APPLICATION_H
#define _SPRITE_APPLICATION_H

#include <system/application.h>
#include <graphics/sprite.h>
#include <graphics/font.h>
#include <input/sony_controller_input_manager.h>
#ifdef _PLATFORM_VITA
#include <audio/vita/audio_manager_vita.h>
#endif
#include <string>

#include "screen_manager.h"

namespace abfw {
class Platform;
class Texture;
class TouchInputManager;
}

class SpriteApplication : public abfw::Application {
public:
	SpriteApplication(abfw::Platform& platform);
	~SpriteApplication();
	void Init();
	void CleanUp();
	bool Update(float ticks);
	void Render();
private:

	abfw::SonyControllerInputManager* controller_manager_;
	abfw::TouchInputManager* touch_manager_;
	
	abfw::Font font_;
	abfw::SpriteRenderer* sprite_renderer_;
	float frame_rate_;

	ScreenManager screen_manager_;

#ifdef _PLATFORM_VITA
	abfw::AudioManagerVita *audio_manager_;
#endif
};

#endif