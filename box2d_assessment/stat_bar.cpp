#include "stat_bar.h"
#include <graphics/colour.h>

// only positioned based on x, if expanded on would have justification, and position size scale etc, but ui is very basic.
const float text_offset_x = 10;

const float percentage_max = 1.0f;
const float percentage_min = 1.0f;


StatBar::StatBar(abfw::Vector3 pos, float width, float height, UInt32 colour){
	
	bar_.set_position(pos);
	bar_.set_width(width);
	bar_.set_height(height);
	bar_.set_colour(colour);
	size_ = width;	

	background_bar_.set_position(pos);
	background_bar_.set_width(width);
	background_bar_.set_height(height);
	
	abfw::Colour i_colour;
	i_colour.SetFromAGBR(colour);
	background_bar_.set_colour(abfw::Colour(i_colour.r, i_colour.g, i_colour.b,0.5f).GetABGR());

	text_position_ = abfw::Vector3((pos.x + size_ / 2) - text_offset_x, pos.y, -0.9f);
	
}
void StatBar::Update(float current,float max){
	// if either of the values have changed update everything
	if (current_ != current || max_ != max) {
		if (current > percentage_min&&current < percentage_max) {
			current = percentage_max;
		}
		current_ = current;
		max_ = max;
		// reset sprite position
		bar_.set_position(bar_.position().x - bar_.width() / 2, bar_.position().y, -0.9f);

		// calculate new width, cur/max *width
		bar_.set_width(size_*current / max);

		// ie move x/y to left edge, then add new width
		bar_.set_position(bar_.position().x + bar_.width() / 2, bar_.position().y, -0.9f);
	}
}

void StatBar::Render(abfw::SpriteRenderer* renderer, abfw::Font *font){
	renderer->DrawSprite(background_bar_);
	renderer->DrawSprite(bar_);
	if (current_ > 0){
		font->RenderText(renderer, text_position_, 0.5f, 0xFFFFFFFF, abfw::TJ_VerticalRight, "%.0f/%.0f", current_, max_);
	}
}