#ifndef _SCREEN_STATE_H
#define _SCREEN_STATE_H

#include <graphics/sprite.h>
#include <graphics/font.h>
#include <input/sony_controller_input_manager.h>


// threading stuff
#ifdef _PLATFORM_VITA
#include <scetypes.h>
#include <kernel.h>		// for scekernelthreadentry
#endif

// predeclare so not included on every .h call
class ScreenManager;
namespace abfw {
class Platform;
}

enum State {	
	kActive,
	kOverlayed,
	kHidden
};
class GameScreen {
public:	
	// allows each seperate screen, to load fonts/images etc.	
	GameScreen(abfw::Platform& platform);
	virtual ~GameScreen();
	virtual void Init() = 0;
	virtual void CleanUp() = 0;
	
	// allows each screen to decide if it should update anything when it is overlayed, 
	// i.e. continue loading assets for a load screen overlay.
	// or disable input if a menu screen, maybe stop rendering ui. in the game
	virtual void Pause(bool is_overlayed);
	virtual void Resume();

	virtual void Update(float dt) = 0;
	virtual void Render(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font_) = 0;
	
	const State GetState(){ return state_; }

	void SetOwner(ScreenManager *screen_manager){ screen_manager_ = screen_manager; }
	virtual bool IsLoading(float &percentage,std::string &text){ return loading_; }

	void set_needs_deleted(bool val) { needs_deleted_ = val; }
	bool const needs_deleted() const { return needs_deleted_; }
protected:
	abfw::Texture* LoadTextureFromPNG(const char* filename);

	State state_;	
	abfw::Platform &platform_;	
	ScreenManager *screen_manager_;
	bool loading_;
	bool needs_deleted_;
	float percentage_;
	
#ifdef _PLATFORM_VITA	
	void LoadStartThread(SceKernelThreadEntry static_function);
	void LoadStopThread();
	SceUID loader_thread_id_;
	bool thread_running_;
#endif
};
#endif