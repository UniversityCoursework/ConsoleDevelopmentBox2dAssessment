#ifndef _OPTION_SCREEN_H
#define _OPTION_SCREEN_H

#include <graphics/sprite.h>

#include <utility>
#include <string>
#include <map>

#include "menu_screen.h"
#include "button.h"
#include "stat_bar.h"

class OptionScreen : public MenuScreen {
public:
	OptionScreen(abfw::Platform& platform);
	void Init() override;
	void CleanUp() override;

	void Update(float dt) override;
	void Render(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font_) override;

protected:
	abfw::Sprite background_;
	
	std::vector<std::pair<std::string, abfw::Vector3>> labels_;
	typedef std::vector<std::pair<std::string, abfw::Vector3>>::iterator Label_It;

	std::vector<StatBar> stat_bars_;
	typedef std::vector<StatBar>::iterator StatBar_It;

	void UseButton(int index) override;

	std::string description_;
};
#endif