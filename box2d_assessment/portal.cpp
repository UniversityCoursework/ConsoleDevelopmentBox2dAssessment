#include "portal.h"

#include "box2d_helpers.h"

void Portal::Init(b2World* world, abfw::Vector3 position, float texture_radius,float collision_radius, abfw::Texture* texture){
	texture_radius *= 2;
	sprite_.set_width(texture_radius);
	sprite_.set_height(texture_radius);
	sprite_.set_position(position);
	sprite_.set_texture(texture);
	CreateStaticBody(world, collision_radius);
	set_type(kPortal);
	is_entered_ = false;
}

void Portal::CreateStaticBody(b2World *world, float collision_radius){
	b2BodyDef body_def;
	body_def.type = b2_staticBody;
	body_def.position = b2Vec2(ABFW_BOX2D_POS_X(sprite_.position().x), ABFW_BOX2D_POS_Y(sprite_.position().y));
	body_ = world->CreateBody(&body_def);

	b2CircleShape circle;
	circle.m_p.SetZero();
	circle.m_radius = ABFW_BOX2D_SIZE(collision_radius);

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circle;
	
	fixtureDef.filter.categoryBits = kPortal;
	fixtureDef.filter.maskBits = kPlayer | kAsteroid;

	body_->CreateFixture(&fixtureDef);

	body_->SetUserData(this);
}
