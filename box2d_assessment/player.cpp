#include "player.h"

#include "box2d_helpers.h"

#include <maths\math_utils.h>
#include <graphics\sprite_renderer.h>
#include <maths\matrix44.h>

const float angular_damping_ = .2f;
const float linear_damping_ = 0.2f;
const float density_ = 40.0f;
const float friction_ = 0.6f;
const float restitution_ = 0.01f;

const float explosion_scale_ = 1.8f;

const float frame_duration_ = 0.08f;
const int num_frames_ = 13;
const float uv_width_ = 0.0769f;
const float uv_offset_x_ = 0.0769f;

const float default_health = 100;
const float bar_offset_y = 30;
const float bar_width = 200;
const float bar_height = 30;
const UInt32 bar_colour = 0xFF79BE00;

const UInt32 panel_colour_vert = 0x3300FF00;
const UInt32 panel_colour_horz = 0x330000FF;

const float survivor_text_x = 10;
const float survivor_text_y = 10;

void Player::Init(b2World* world, abfw::Vector3 positon, float radius, float collision_radius, abfw::Texture* texture, abfw::Texture* explosion_texture){
	radius *= 2;
	sprite_.set_width(radius);
	sprite_.set_height(radius);
	sprite_.set_position(positon);
	sprite_.set_texture(texture);

	explosion_sprite_.set_width(radius*explosion_scale_);
	explosion_sprite_.set_height(radius*explosion_scale_);
	explosion_sprite_.set_position(positon);
	explosion_sprite_.set_texture(explosion_texture);

	explosion_sprite_.set_anim_frame_duration(frame_duration_);
	explosion_sprite_.set_num_frames(num_frames_);
	explosion_sprite_.set_uv_offset(abfw::Vector2(uv_offset_x_, 0));
	explosion_sprite_.set_uv_width(uv_width_);
		
	CreateDynamicBody(world, collision_radius);
	set_type(kPlayer);
	health_bar_ = StatBar(abfw::Vector3(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT - bar_offset_y, -0.9f), bar_width, bar_height, bar_colour);
	health_ = default_health;
	force_strength_ = .0f;
	max_health_ = health_;
	is_alive_ = true;	
	score_ = static_cast<int>(max_health_);
	show_panels_ = false;

	BuildForcePanels();
}

void Player::UpdateInput(abfw::TouchInputManager* touch_manager){
	for (int touch_num = 0; touch_num < touch_manager->max_num_touches(); ++touch_num) {
		const abfw::Touch& touch = touch_manager->GetTouch(0, touch_num);
		if (touch.type != abfw::TT_NONE) {
			for (int i = 0; i < force_panels_.size(); i++) {
				if (force_panels_[i].ContainsPoint(abfw::Vector2(touch.position.x, touch.position.y))) {
					UseForcePanel((ForceDirection)i);
				}
			}
		}
	}
}

void Player::Update(float dt){
	if (is_alive_){
		b2Vec2 ship_pos = body_->GetPosition();
		sprite_.set_position(BOX2D_ABFW_POS_X(body_->GetPosition().x), BOX2D_ABFW_POS_Y(body_->GetPosition().y), sprite_.position().z);
		sprite_.set_rotation(-body_->GetAngle());

	}
	else if (explosion_sprite_.UpdateAnimation(dt)){
		// animation ended so flag for deletion		
		needs_deleted_ = true;
	}
	health_bar_.Update(health_, max_health_);

}

void Player::Render(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font){
	if (is_alive()) {
		sprite_renderer->DrawSprite(sprite_);
	}
	else {
		sprite_renderer->DrawSprite(explosion_sprite_);
	}
}

void Player::RenderHud(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font){
	health_bar_.Render(sprite_renderer, font);
	font->RenderText(sprite_renderer, abfw::Vector3(survivor_text_x, survivor_text_y, -0.9f), 1.0f, 0xFFFFFFFF, abfw::TJ_LEFT, "Survivors: %06d", score_);
	if (show_panels_){
		for (int i = 0; i < debug_tp_.size(); i++) {
			sprite_renderer->DrawSprite(debug_tp_[i]);
		}
	}

}

void Player::BlowUp() {
	if (is_alive_) {
		is_alive_ = false;
		explosion_sprite_.set_position(BOX2D_ABFW_POS_X(body_->GetPosition().x), BOX2D_ABFW_POS_Y(body_->GetPosition().y), sprite_.position().z);
	}
}

void Player::CreateDynamicBody(b2World *world, float collision_radius){
	// dynamic body
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position = b2Vec2(ABFW_BOX2D_POS_X(sprite_.position().x), ABFW_BOX2D_POS_Y(sprite_.position().y));
	bodyDef.angularDamping = angular_damping_;
	bodyDef.linearDamping = linear_damping_;
	// add to world and store pointer to it
	body_ = world->CreateBody(&bodyDef);

	b2CircleShape circle;
	circle.m_p.SetZero();
	circle.m_radius = ABFW_BOX2D_SIZE(collision_radius);

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circle;
	fixtureDef.density = density_;
	fixtureDef.friction = friction_;
	fixtureDef.restitution = restitution_;

	fixtureDef.filter.categoryBits = kPlayer;
	fixtureDef.filter.maskBits = kPickUp | kAsteroid | kPlanet | kPortal;

	body_->CreateFixture(&fixtureDef);

	body_->SetUserData(this);
}

void Player::BuildForcePanels() {
	float tp_width = DISPLAY_WIDTH / 6;
	float tp_height = DISPLAY_HEIGHT / 6;
	for (int i = 0; i < force_panels_.size(); i++) {
		switch ((ForceDirection)i) {
			case Player::kT_Top:
				force_panels_[i] = Rectangle(tp_width * 3, tp_height, 3 * tp_width, tp_height);
				debug_tp_[i].set_position(abfw::Vector3(force_panels_[i].x(), force_panels_[i].y(), -0.92f));
				debug_tp_[i].set_width(2 * force_panels_[i].half_width());
				debug_tp_[i].set_height(2 * force_panels_[i].half_height());
				debug_tp_[i].set_colour(panel_colour_horz);
				break;
			case Player::kT_Left:
				force_panels_[i] = Rectangle(tp_width, tp_height * 3, tp_width, 3 * tp_height);
				debug_tp_[i].set_position(abfw::Vector3(force_panels_[i].x(), force_panels_[i].y(), -0.92f));
				debug_tp_[i].set_width(2 * force_panels_[i].half_width());
				debug_tp_[i].set_height(2 * force_panels_[i].half_height());
				debug_tp_[i].set_colour(panel_colour_vert);
				break;
			case Player::kT_Right:
				force_panels_[i] = Rectangle(tp_width * 5, tp_height * 3, tp_width, 3 * tp_height);
				debug_tp_[i].set_position(abfw::Vector3(force_panels_[i].x(), force_panels_[i].y(), -0.92f));
				debug_tp_[i].set_width(2 * force_panels_[i].half_width());
				debug_tp_[i].set_height(2 * force_panels_[i].half_height());
				debug_tp_[i].set_colour(panel_colour_vert);
				break;
			case Player::kT_Bottom:
				force_panels_[i] = Rectangle(tp_width * 3, tp_height * 5, 3 * tp_width, tp_height);
				debug_tp_[i].set_position(abfw::Vector3(force_panels_[i].x(), force_panels_[i].y(), -0.92f));
				debug_tp_[i].set_width(2 * force_panels_[i].half_width());
				debug_tp_[i].set_height(2 * force_panels_[i].half_height());
				debug_tp_[i].set_colour(panel_colour_horz);
				break;
			default:
				break;
		}
	}
}

void Player::UseForcePanel(ForceDirection direction){	
	switch (direction) {		
		case Player::kT_Top:
			ApplyForce(abfw::Vector3(0, (sprite_.height() / 2), 0.0f), 
					   abfw::Vector3(0, -1, 0));
			break;
		case Player::kT_Left:
			ApplyForce(abfw::Vector3(-(sprite_.width() / 2), 0, 0.0f),
					   abfw::Vector3(1, 0.f, 0.0f));
			break;
		case Player::kT_Right:
			ApplyForce(abfw::Vector3((sprite_.width() / 2), 0, 0.0f), 
					   abfw::Vector3(-1, 0, 0));
			break;		
		case Player::kT_Bottom:
			ApplyForce(abfw::Vector3(0, -(sprite_.height() / 2), 0.0f), 
						abfw::Vector3(0, 1, 0));
			break;
		default:
			break;
	}
}

void Player::ApplyForce(abfw::Vector3 force_pos, abfw::Vector3 force_direction) {
	force_pos += abfw::Vector3(sprite_.position().x, sprite_.position().y, 0);
	body_->ApplyForce(force_strength_*b2Vec2(force_direction.x, force_direction.y),
		b2Vec2(ABFW_BOX2D_POS_X(force_pos.x), ABFW_BOX2D_POS_Y(force_pos.y)));
}
