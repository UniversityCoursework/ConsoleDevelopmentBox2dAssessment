#include "option_screen.h"

#include <sstream>

#include <input\touch_input_manager.h>
#include <system/platform.h>

#include "screen_manager.h"
#include "game_settings.h"

// this should really be done with an xml parser or something
enum ButtonName{
	kMaster_Up=0,
	kMaster_Down,
	kMusic_Up,
	kMusic_Down,
	kSFX_Up,
	kSFX_Down,
	kThruster_Up,
	kThruster_Down,
	kBack
};
enum StatBarName {
	kMasterStatBar = 0,
	kMusicStatBar,
	kSFXStatBar
};

const float button_width = 180;
const float button_height = 60;

const float vol_width = 50;
const float vol_height = 50;
const float vol_offset_x = -420;
const float vol_offset_y = 54;

const float up_offset_x = 238;

const float label_offsets_ = 200;
const float controls_start = 200;

const float button_offset_x = 94;
const float button_offset_y = 128;
const float title_offset = 120;

const float bg_height_offset = 200;

const UInt32 bg_colour = 0x805D1372;
const UInt32 button_bg_colour = 0x00CFA037;
const UInt32 button_highlight_colour = 0x7DFFFFFF;

const float volume_increment = 0.05f;
const float thruster_increment = .5f;

const float stat_bar_offset_x = -300;
const float stat_bar_width = 180.f;
const float stat_bar_height = 32.f;
const UInt32 stat_bar_colour = 0xFFCFA037;

const float description_offset_y = 40;

OptionScreen::OptionScreen(abfw::Platform& platform)
:MenuScreen(platform) {
}

void OptionScreen::Init() {
	background_.set_position(platform_.width() / 2, platform_.height() / 2, -0.9f);
	background_.set_width(platform_.width());
	background_.set_height(platform_.height() - bg_height_offset);
	background_.set_colour(bg_colour);
	description_ = "";

	current_button_ = -1;

	labels_.push_back(std::make_pair("Master Volume", abfw::Vector3(label_offsets_, controls_start, -0.91f)));

	//kMaster_Up
	buttons_.push_back(Button(abfw::Vector3(platform_.width() + vol_offset_x + up_offset_x,
						controls_start,
						-0.9f), vol_width, vol_height, button_bg_colour, button_highlight_colour, ">"));	
	//kMasterStatBar
	stat_bars_.push_back(StatBar(abfw::Vector3(platform_.width() + stat_bar_offset_x,
		controls_start,
		-0.9f), stat_bar_width, stat_bar_height, stat_bar_colour));	
	//kMaster_Down
	buttons_.push_back(Button(abfw::Vector3(platform_.width() + vol_offset_x,
		controls_start,
		-0.9f), vol_width, vol_height, button_bg_colour, button_highlight_colour, "<"));

	labels_.push_back(std::make_pair("Music Volume",abfw::Vector3(label_offsets_, controls_start + vol_offset_y, -0.91f)));
	//kMusic_Up
	buttons_.push_back(Button(abfw::Vector3(platform_.width() + vol_offset_x + up_offset_x,
		controls_start + vol_offset_y,
		-0.9f), vol_width, vol_height, button_bg_colour, button_highlight_colour, ">"));
	//kMusicStatBar
	stat_bars_.push_back(StatBar(abfw::Vector3(platform_.width() + stat_bar_offset_x,
		controls_start + vol_offset_y,
		-0.9f), stat_bar_width, stat_bar_height, stat_bar_colour));
	//kMusic_Down
	buttons_.push_back(Button(abfw::Vector3(platform_.width() + vol_offset_x,
		controls_start + vol_offset_y,
		-0.9f), vol_width, vol_height, button_bg_colour, button_highlight_colour, "<"));
	
	labels_.push_back(std::make_pair("SFX Volume",abfw::Vector3(label_offsets_, controls_start + 2 * vol_offset_y, -0.91f)));
	//kSFX_Up
	buttons_.push_back(Button(abfw::Vector3(platform_.width() + vol_offset_x + up_offset_x,
												controls_start + 2 * vol_offset_y,
								 -0.9f), vol_width, vol_height, button_bg_colour, button_highlight_colour, ">"));
	//kSFXStatBar
	stat_bars_.push_back(StatBar(abfw::Vector3(platform_.width() + stat_bar_offset_x,
										controls_start + 2 * vol_offset_y,
										-0.9f), stat_bar_width, stat_bar_height, stat_bar_colour));
	//kSFX_Down
	buttons_.push_back(Button(abfw::Vector3(platform_.width() + vol_offset_x,
												controls_start + 2 * vol_offset_y,
								   -0.9f), vol_width, vol_height, button_bg_colour, button_highlight_colour, "<"));
	
	labels_.push_back(std::make_pair("Thruster Strength",abfw::Vector3(label_offsets_, controls_start + 3 * vol_offset_y, -0.91f)));

	//kThruster_Up
	buttons_.push_back(Button(abfw::Vector3(platform_.width() + vol_offset_x + up_offset_x,
		controls_start + 3 * vol_offset_y,
		-0.9f), vol_width, vol_height, button_bg_colour, button_highlight_colour, ">"));

	GameSettings* settings = static_cast<GameSettings* >(screen_manager_->GetSettings());
	labels_.push_back(std::make_pair(static_cast<std::ostringstream*>(&(std::ostringstream() << settings->force_strength()))->str()
		, abfw::Vector3(platform_.width() + stat_bar_offset_x,
		controls_start + 3 * vol_offset_y,
		-0.9f)));

	//kThruster_Down
	buttons_.push_back(Button(abfw::Vector3(platform_.width() + vol_offset_x,
		controls_start + 3 * vol_offset_y,
		-0.9f), vol_width, vol_height, button_bg_colour, button_highlight_colour, "<"));

	//kBack
	buttons_.push_back(Button(abfw::Vector3(button_offset_x, platform_.height() - button_offset_y, -0.9f), button_width, button_height, button_bg_colour, button_highlight_colour, "Back"));
}

void OptionScreen::CleanUp() {	
}

void OptionScreen::Update(float dt) {
	switch(state_) {
		case kActive: {
				UpdateMenu(dt);
				Settings* settings = screen_manager_->GetSettings();
				// *100 so percentage
				stat_bars_[kMasterStatBar].Update(settings->master_volume()*100.f, 100.0f);
				stat_bars_[kMusicStatBar].Update(settings->music_volume()*100.f, 100.0f);
				stat_bars_[kSFXStatBar].Update(settings->sfx_volume()*100.f, 100.0f);
				break;
			}
		case kOverlayed: {
				// any background animations, updates etc.
				break;
			}
		default:
			break;
	}
}

void OptionScreen::UseButton(int index) {	
	GameSettings* settings = static_cast<GameSettings* >(screen_manager_->GetSettings());
	switch(ButtonName(index)) {
		case kMaster_Up: {
				settings->set_master_volume(settings->master_volume() + volume_increment);
				#ifdef _PLATFORM_VITA
				screen_manager_->GetAudioManager()->SetMasterVolume(settings->master_volume());
				#endif
				description_ = "Master Volume - Up";
				break;
			}
		case kMaster_Down: {
				settings->set_master_volume(settings->master_volume() - volume_increment);
				#ifdef _PLATFORM_VITA
				screen_manager_->GetAudioManager()->SetMasterVolume(settings->master_volume());
				#endif
				description_ = "Master Volume - Down";
				break;
			}
		case kMusic_Up: {
				settings->set_music_volume(settings->music_volume() + volume_increment);
				#ifdef _PLATFORM_VITA
				abfw::VolumeInfo volume_info;
				volume_info.volume = settings->music_volume();
				screen_manager_->GetAudioManager()->SetMusicVolumeInfo(volume_info);
				#endif
				description_ = "Music Volume - Up";
				break;
			}
		case kMusic_Down: {
				settings->set_music_volume(settings->music_volume() - volume_increment);
				#ifdef _PLATFORM_VITA
				abfw::VolumeInfo volume_info;
				volume_info.volume = settings->music_volume();
				screen_manager_->GetAudioManager()->SetMusicVolumeInfo(volume_info);
				#endif
				description_ = "Music Volume - Down";
				break;
			}
		case kSFX_Up: {
				settings->set_sfx_volume(settings->sfx_volume() + volume_increment);
				description_ = "Sound FX Volume - Up";
				break;
			}
		case kSFX_Down: {
				settings->set_sfx_volume(settings->sfx_volume() - volume_increment);
				description_ = "Sound FX Volume - Down";
				break;
			}
		case kThruster_Up:{
				settings->set_force_strength(settings->force_strength() + thruster_increment);
				labels_[4].first = static_cast<std::ostringstream*>(&(std::ostringstream() << settings->force_strength()))->str();
				
				description_ = "Strength of the force applied, higher value less points. - Up";
				break;
			}
		case kThruster_Down:{
				settings->set_force_strength(settings->force_strength() - thruster_increment);
				labels_[4].first = static_cast<std::ostringstream*>(&(std::ostringstream() << settings->force_strength()))->str();
				description_ = "Strength of the force applied, higher value less points. - Down";
				break;
			}
		case kBack: {
				screen_manager_->PopScreen();
				break;
			}
		default:
			// incase you mucked up
			break;
	}
}

void OptionScreen::Render(abfw::SpriteRenderer * sprite_renderer, abfw::Font * font) {
	switch (state_) {
	case kActive:
		sprite_renderer->DrawSprite(background_);
		font->RenderText(sprite_renderer, abfw::Vector3(platform_.width() / 2, title_offset, -0.9f), 1.0f, 0xffffffff, abfw::TJ_CENTRE, "Options Screen");
		for (Button_It button = buttons_.begin(); button != buttons_.end(); button++){
			button->Render(sprite_renderer, font);
		}
		for (StatBar_It stat_bar = stat_bars_.begin(); stat_bar != stat_bars_.end(); stat_bar++){
			stat_bar->Render(sprite_renderer, font);
		}
		for (Label_It label = labels_.begin(); label != labels_.end(); label++){
			font->RenderText(sprite_renderer, label->second, 1.0f, 0xFFFFFFFF, abfw::TJ_VerticalLeft, label->first.c_str());
		}

		font->RenderText(sprite_renderer, abfw::Vector3(platform_.width() / 2, platform_.height() - description_offset_y, -0.9f), 1.0f, 0xffffffff, abfw::TJ_CENTRE, description_.c_str());
		break;
	case kOverlayed:
		break;
	default:
		break;
	}
}
