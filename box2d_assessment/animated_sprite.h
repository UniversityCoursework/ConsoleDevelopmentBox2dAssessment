#ifndef _ANIMATED_SPRITE_H
#define _ANIMATED_SPRITE_H

#include <graphics/sprite.h>

class AnimatedSprite : public abfw::Sprite {
public:
	AnimatedSprite();
	// returns true when last frame /out of bounds
	bool UpdateAnimation(float dt);

	// how much to move along each frame
	inline void set_uv_offset(abfw::Vector2 offset) { uv_offset_ = offset; }
	inline const abfw::Vector2 uv_offset() { return uv_offset_; }

	// time in secs a single aniamtion frame is on screen
	inline void set_anim_frame_duration(float val) { anim_frame_duration_ = val; }
	inline const float anim_frame_duration() const { return anim_frame_duration_; }

	// animation time
	inline void set_anim_time(float val) { anim_time_ = val; }
	inline const float anim_time() const { return anim_time_; }

	// number of animation frames
	inline void set_num_frames(int val) { num_frames_ = val; }
	inline const int num_frames() const { return num_frames_; }

protected:
	float anim_frame_duration_;	
	float anim_time_;
	int num_frames_;

	bool looping_;

	abfw::Vector2 uv_offset_;
	

};

#endif