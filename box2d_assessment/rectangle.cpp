#include "rectangle.h"
Rectangle::Rectangle() {
	x_ = 0;
	y_ = 0;
	half_width_ = 0;
	half_height_ = 0;
}

Rectangle::Rectangle(float x, float y, float half_width, float half_height) {
	x_ = x;
	y_ = y;
	half_width_ = half_width;
	half_height_ = half_height;
}

bool Rectangle::ContainsPoint(const abfw::Vector2 &point){
	if((x_ + half_width_) < (point.x)) return false;
	if((x_ - half_width_) > (point.x)) return false;
	if((y_ + half_height_) < (point.y)) return false;
	if((y_ - half_height_) > (point.y)) return false;
	return true;
}
