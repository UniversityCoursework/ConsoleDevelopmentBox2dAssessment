#ifndef _PORTAL_H
#define _PORTAL_H

#include <maths\vector3.h>
#include <graphics\texture.h>
#include <graphics\sprite.h>

#include "game_object.h"

class Portal :public GameObject{
public:
	Portal(){};
	void Init(b2World* world, abfw::Vector3 position, float texture_radius, float collision_radius, abfw::Texture* texture);
	
	void set_is_entered(bool val) { is_entered_ = val; }
	bool is_entered() { return is_entered_; }

	abfw::Sprite const Sprite() const { return sprite_; }
protected:

	void CreateStaticBody(b2World *world, float collision_radius);

	bool is_entered_;

	abfw::Sprite sprite_;

};
#endif