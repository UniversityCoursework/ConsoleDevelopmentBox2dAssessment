#include "game_boundary.h"

#include "Box2D\Box2D.h"
#include "box2d_helpers.h"

const float density_ = 10.0f;
const float friction_ = 1.0f;
const float restitution_ = 0.0f;

GameBoundary::GameBoundary(b2World * world, abfw::Vector3 position, float width, float height, UInt32 colour){
	sprite_.set_width(width);
	sprite_.set_height(height);
	sprite_.set_position(position);
	sprite_.set_colour(colour);
	
	CreateStaticBody(world, width,height);
	set_type(kBoundary);
}
GameBoundary::~GameBoundary() {
	body_->GetWorld()->DestroyBody(body_);
}

void GameBoundary::CreateStaticBody(b2World * world, float width,float height) {
	b2BodyDef body_def;
	body_def.type = b2_staticBody;
	body_def.position = b2Vec2(ABFW_BOX2D_POS_X(sprite_.position().x), ABFW_BOX2D_POS_Y(sprite_.position().y));
	body_ = world->CreateBody(&body_def);

	b2PolygonShape shape;
	shape.SetAsBox(ABFW_BOX2D_SIZE(width / 2), ABFW_BOX2D_SIZE(height / 2));

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = density_;
	fixtureDef.friction = friction_;
	fixtureDef.restitution = restitution_;
	fixtureDef.filter.categoryBits = kBoundary;
	fixtureDef.filter.maskBits = kPlayer;

	body_->CreateFixture(&fixtureDef);

	body_->SetUserData(this);
}
void GameBoundary::Render(abfw::SpriteRenderer *sprite_renderer){
	sprite_renderer->DrawSprite(sprite_);
}
