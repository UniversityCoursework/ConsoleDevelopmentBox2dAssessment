#ifndef _ASTEROID_H
#define _ASTEROID_H

#include <graphics/texture.h>
#include <maths/vector3.h>
#include <graphics/sprite.h>
#include <graphics\sprite_renderer.h>

#include "game_object.h"
#include "animated_sprite.h"

class Asteroid: public GameObject{
public:
	Asteroid();
	~Asteroid();
	Asteroid(b2World* world, abfw::Vector3 positon, float radius, float collision_radius, b2Vec2 direction, abfw::Texture* base_texture, abfw::Texture * explosion_texture);

	float GetDamage();	
	void BlowUp();

	void Update(float dt) override;
	
	void set_is_alive(bool val) { is_alive_ = val; }
	bool is_alive() { return is_alive_; }

	void Render(abfw::SpriteRenderer *sprite_renderer);
		
protected:
	abfw::Sprite sprite_;
	AnimatedSprite explosion_sprite_;

	void CreateDynamicBody(b2World *world, float collision_radius);

	bool is_alive_;

};

#endif