#ifndef _MAIN_MENU_H
#define _MAIN_MENU_H

#include "menu_screen.h"
#include "button.h"
#include "shared_assets.h"

class MainMenu : public MenuScreen {
public:	
	MainMenu(abfw::Platform& platform);

	void Init() override;
	void Load();
	void CleanUp() override;
	bool IsLoading(float &percentage, std::string &text) override;

	void Update(float dt) override;	
	void Render(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font_) override;

protected:	
	abfw::Sprite banner_;
	abfw::Sprite background_;
	SharedAssets shared_assets_;
	std::string currently_loading_;
	
	void UseButton(int index) override;	
	
};
#endif