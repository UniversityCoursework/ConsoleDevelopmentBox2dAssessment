#include "pause_screen.h"


#include <input\touch_input_manager.h>
#include <system/platform.h>

#include "screen_manager.h"
#include "option_screen.h"
#include "tutorial_screen.h"

enum ButtonName{
	kResume=0,
	kTutorial,
	kOptions,
	kExit
};

const float button_width = 280;
const float button_height = 60;
const float button_offset = 62;
const float button_start = 186;
const float title_offset = 120;

const float bg_height_offset = 200;
const UInt32 bg_colour = 0x805D1372;

PauseScreen::PauseScreen(abfw::Platform& platform, SharedAssets &shared_assets)
:MenuScreen(platform),shared_assets_(shared_assets){

}

void PauseScreen::Init() {
	background_.set_position(platform_.width() / 2, platform_.height() / 2, -0.9f);
	background_.set_width(platform_.width());
	background_.set_height(platform_.height() - bg_height_offset);
	background_.set_colour(bg_colour);

	current_button_ = kResume;	
	// kResume
	buttons_.push_back(Button(abfw::Vector3(platform_.width() / 2, button_start , -0.9f), button_width, button_height, 0xFFCFA037, 0x7DFFFFFF, "Resume"));
	// kTutorial
	buttons_.push_back(Button(abfw::Vector3(platform_.width() / 2, button_start + button_offset, -0.9f), button_width, button_height, 0xFFCFA037, 0x7DFFFFFF, "Tutorials"));
	// kOptions
	buttons_.push_back(Button(abfw::Vector3(platform_.width() / 2, button_start + 2*button_offset, -0.9f), button_width, button_height, 0xFFCFA037, 0x7DFFFFFF, "Options"));
	// kExit
	buttons_.push_back(Button(abfw::Vector3(platform_.width() / 2, button_start + 3*button_offset, -0.9f), button_width, button_height, 0xFFCFA037, 0x7DFFFFFF, "Exit"));
}

void PauseScreen::CleanUp() {	
}

void PauseScreen::Update(float dt) {
	switch(state_) {
		case kActive: {
				UpdateMenu(dt);
				break;
			}
		case kOverlayed: {
				// any background animations, updates etc.
				break;
			}
		default:
			break;
	}
}

void PauseScreen::UseButton(int index) {
	switch(ButtonName(index)) {
		case kResume: {
				screen_manager_->PopScreen();
				break;
			}
		case kOptions: {
				screen_manager_->PushScreen(new OptionScreen(platform_));
				Pause(false);
				break;
			}
		case kTutorial: {
				screen_manager_->PushScreen(new TutorialScreen(platform_,shared_assets_));
				Pause(false);
				break;
			}
		case kExit: {
				screen_manager_->PopScreen(2);
				break;
			}
		default:
			break;
	}
}

void PauseScreen::Render(abfw::SpriteRenderer * sprite_renderer, abfw::Font * font_) {	
	switch(state_) {
		case kActive:
			sprite_renderer->DrawSprite(background_);
			font_->RenderText(sprite_renderer, abfw::Vector3(platform_.width() / 2, title_offset, -0.9f), 1.0f, 0xffffffff, abfw::TJ_CENTRE, "Pause Screen");
			for (int i = 0; i <buttons_.size(); i++) {
				buttons_[i].Render(sprite_renderer, font_);
			}			
			break;
		case kOverlayed:			
			break;
		default:
			break;
	}
}
