#ifndef _PICKUP_MANAGER_H
#define _PICKUP_MANAGER_H

#include <graphics/sprite_renderer.h>

#include <vector>

#include "pick_up.h"

class PickUpManager {
public:
	void CleanUp();

	void AddPickUp(PickUp *pickup);
	void Update(float dt);
	void Render(abfw::SpriteRenderer *renderer);

	std::vector<PickUp* > &GetPickUps();
private:
	std::vector<PickUp* > pickups_;
	typedef std::vector<PickUp*>::iterator PickUp_It;
};

#endif
