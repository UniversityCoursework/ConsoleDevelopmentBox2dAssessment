#include "planet_manager.h"

void PlanetManager::CleanUp() {
	// Spawners
	for (Planet_it planet = planets_.begin(); planet != planets_.end(); planet++) {
		delete(*planet);
		(*planet) = nullptr;
	}
	planets_.clear();
}

void PlanetManager::AddPlanet(Planet * planet) {
	planets_.push_back(planet);
}

void PlanetManager::Update(float dt) {
	for (Planet_it planet = planets_.begin(); planet != planets_.end(); ++planet) {
		(*planet)->Update(dt);
	}
}

void PlanetManager::Render(abfw::SpriteRenderer * renderer) {
	for (Planet_it planet = planets_.begin(); planet != planets_.end(); ++planet) {
		(*planet)->Render(renderer);
	}
}

std::vector<Planet*>& PlanetManager::GetPlanets() {
	return planets_;
}
