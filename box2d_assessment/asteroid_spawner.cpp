#include "asteroid_spawner.h"

AsteroidSpawner::AsteroidSpawner(b2World * world, abfw::Vector3 position, float radius, float collision_radius, b2Vec2 direction, abfw::Texture * texture, abfw::Texture * explosion_texture) {
	// defaults just incase
	asteroids_ = nullptr;
	spawn_timer_ = 0;
	spawn_delay_ = 0;

	world_ = world;

	position_ = position;
	radius_ = radius;
	collision_radius_ = collision_radius;
	direction_ = direction;
	texture_ = texture;	
	t_explosion_ = explosion_texture;
}

void AsteroidSpawner::Update(float dt) {
	spawn_timer_ += dt;
	if(spawn_timer_ > spawn_delay_) {		
		Spawn();	
		spawn_timer_ = 0;		
	}
}

void AsteroidSpawner::Spawn() {		
	asteroids_->push_back(new Asteroid(world_, position_, radius_, collision_radius_, direction_, texture_, t_explosion_));
}

