#ifndef _LOADING_SCREEN_H
#define _LOADING_SCREEN_H

#include "game_screen.h"

#include "animated_sprite.h"

class LoadingScreen : public GameScreen{
public:
	LoadingScreen(abfw::Platform& platform) :
	GameScreen(platform) {
	}
	void Init() override;	
	void CleanUp() override;

	void Update(float dt) override;
	void Render(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font_) override;

protected:
	abfw::Sprite background_;
	abfw::Sprite dodad_;
	//AnimatedSprite dodad_;
	float percentage_;
	std::string description_;
	
};

#endif