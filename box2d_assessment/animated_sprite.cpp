#include "animated_sprite.h"

AnimatedSprite::AnimatedSprite() {
	anim_time_ = 0;
	anim_frame_duration_ = -1;
	num_frames_ = 0;
	uv_offset_ = abfw::Vector2(0, 0);
	looping_ = false;
}

bool AnimatedSprite::UpdateAnimation(float dt) {
	//increment time passed
	anim_time_ += dt;
	if(anim_time_ > anim_frame_duration_) {
		//reset
		anim_time_ = 0;

		// move texture coords
		uv_position_ += uv_offset_;

		// check valid 
		if(uv_position_.y > (num_frames_ - 1) *uv_height_) {
			if (looping_){
				uv_position_.y = 0;
			}else {
				uv_position_.y -= uv_offset_.y;
			}
			return true;
		}
		if(uv_position_.x > (num_frames_ - 1) *uv_width_) {
			if (looping_){
				uv_position_.x = 0;
			}else{
				uv_position_.x -= uv_offset_.x;
			}
			return true;
		}		
	}
	return false;
}