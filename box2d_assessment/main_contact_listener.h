#ifndef _MAIN_CONTACT_LISTENER_H
#define _MAIN_CONTACT_LISTENER_H

#include <Box2D/Box2D.h>

#include "box2d_helpers.h"

#ifdef _PLATFORM_VITA
#include <audio/vita/audio_manager_vita.h>
#endif
#include "game_settings.h"


class MainContactListener : public b2ContactListener {

public:	
	
#ifdef _PLATFORM_VITA
	void Init(abfw::AudioManagerVita *audio_manager_, GameSettings* game_settings, int pickup_id, int explosion_id_);

	void PlayPickUpSFX();
	void PlayExplosionSFX();
#else 
	void Init(GameSettings* game_settings);
#endif
	/// Called when two fixtures begin to touch.
	void BeginContact(b2Contact* contact);

	/// Called when two fixtures cease to touch.
	//void EndContact(b2Contact* contact);

	/// This lets you inspect a contact after the solver is finished. This is useful
	/// for inspecting impulses.
	/// Note: the contact manifold does not include time of impact impulses, which can be
	/// arbitrarily large if the sub-step is small. Hence the impulse is provided explicitly
	/// in a separate data structure.
	/// Note: this is only called for contacts that are touching, solid, and awake.
	void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse);
protected:	

#ifdef _PLATFORM_VITA
	abfw::AudioManagerVita *audio_manager_;	
	int pickup_id_;
	int explosion_id_;
#endif
	GameSettings* game_settings_;	

};

#endif