#include "level.h"

#include <system/Platform.h>

#include <cmath>

#include "screen_manager.h"
#include "pause_screen.h"
#include "loading_screen.h"
#include "win_screen.h"
#include "loser_screen.h"

#include "asteroid_spawner.h"

#include "asteroid.h"
#include "planet.h"

const UInt32 boundary_colour = 0x33BABABA;
const float boundary_thickness = 40;

const float background_width = 4096;
const float background_height = 2048;

// allows multi threading of the load on vita
#ifdef _PLATFORM_VITA
#include <stdio.h>
static Int32 LoadGameThread(SceSize args, void *argc) {
	Level* level = *(Level**)argc;
	level->Load();
	return 0;
}
#endif

void Level::Init() {
#ifdef _PLATFORM_VITA	
	LoadStartThread(LoadGameThread);
#else	
	Load();
#endif
}

void Level::Load() {
	percentage_ = 0.0f;
	game_settings_ = static_cast<GameSettings*>(screen_manager_->GetSettings());
	follow_cam = true;
#ifdef _PLATFORM_VITA	
	currently_loading_ = "All the Explosions!!!";
	explosion_sfx_id = screen_manager_->GetAudioManager()->LoadSample("explodemini.wav", platform_);
	percentage_ += 0.1f;
	pick_up_sfx_id_ = screen_manager_->GetAudioManager()->LoadSample("pick_up.wav", platform_);	
	pick_up_voice_ = -1;
	percentage_ += 0.1f;	
	contact_listener_.Init(screen_manager_->GetAudioManager(), game_settings_, pick_up_sfx_id_,explosion_sfx_id);
#else 	
	contact_listener_.Init(game_settings_);
	percentage_ += 0.2f;
#endif	
	currently_loading_ = "Asteroids";
	t_asteroid_ = LoadTextureFromPNG("bin/asteroid_1.png");
	percentage_ += 0.1f;
	currently_loading_ = "Gravity";
	t_gravity_well_ = LoadTextureFromPNG("bin/gravity_well.png");
	percentage_ += 0.1f;
	currently_loading_ = "Fiery swirly mabob...ehmm explosions?";
	t_explosion_ = LoadTextureFromPNG("bin/explosion.png");
	percentage_ += 0.1f;
	
	LoadLevel();
	
	// Create the Box2D physics world
	b2Vec2 gravity(0.0f, 0.0f);
	world_ = new b2World(gravity);

	world_->SetContactListener(&contact_listener_);

	// Build all the things
	background_.set_width(background_width);
	background_.set_height(background_height);

	background_.set_position(background_.width() / 2, background_.height()/2, -0.58f);
	background_.set_texture(shared_assets_.t_background);
	percentage_ += 0.1f;

	currently_loading_ = "Limits of space";
	float offset_x = background_.width() - (platform_.width() / 2);
	float offset_y = background_.height() - (platform_.height() / 2);

	float boundary_width = background_.width() - platform_.width();
	float boundary_height = background_.height() - platform_.height();
	
	boundary_manager_.AddGameEdge(new GameBoundary(world_,
		abfw::Vector3(platform_.width() / 2, background_.height() / 2, -0.6f),
		boundary_thickness, boundary_height + boundary_thickness, boundary_colour));
	boundary_manager_.AddGameEdge(new GameBoundary(world_,
		abfw::Vector3(background_.width() - (platform_.width() / 2), background_.height() / 2, -0.6f),
		boundary_thickness, boundary_height + boundary_thickness, boundary_colour));

	boundary_manager_.AddGameEdge(new GameBoundary(world_,
		abfw::Vector3(background_.width() / 2, platform_.height() / 2, -0.6f),
		boundary_width - boundary_thickness, boundary_thickness, boundary_colour));
	boundary_manager_.AddGameEdge(new GameBoundary(world_,
		abfw::Vector3(background_.width() / 2, background_.height() - (platform_.height() / 2), -0.6f),
		boundary_width - boundary_thickness, boundary_thickness, boundary_colour));

	BuildLevel();
	loading_ = false;	
}

void Level::CleanUp(){
	// free up memory from textures
	CleanUpLevel();
	delete t_asteroid_;
	t_asteroid_ = nullptr;
	
	delete t_explosion_;
	t_explosion_ = nullptr;	

	asteroid_manager_.CleanUp();
	planet_manager_.CleanUp();
	pickup_manager_.CleanUp();
	boundary_manager_.CleanUp();
	
	delete world_;
	world_ = nullptr;
#ifdef _PLATFORM_VITA	
	screen_manager_->GetAudioManager()->UnloadSample(explosion_sfx_id);	
	screen_manager_->GetAudioManager()->UnloadSample(pick_up_sfx_id_);	
#endif
}

bool Level::IsLoading(float &percentage, std::string &text){
	percentage = percentage_;
	text = currently_loading_;
	return loading_;
}

void Level::Pause(bool is_overlayed){
	if (is_overlayed) {
		state_ = kOverlayed;
	}
	else {
		state_ = kHidden;
	}
	StopAudio();
}
void Level::Resume(){
	state_ = kActive;
	ResumeAudio();
	spaceship_.set_force_strength(game_settings_->force_strength());
}
void Level::Update(float dt){
	switch(state_) {
		case kActive: {
//#ifndef _PLATFORM_VITA
//				const abfw::Touch& first_touch = screen_manager_->GetTouchManager()->GetTouch(0, 0);
//				if (first_touch.type == abfw::TT_ACTIVE){
//					screen_manager_->PushScreen(new PauseScreen(platform_));
//					Pause(true);
//				}
//#endif
				const abfw::SonyController* controller = screen_manager_->GetControllerManager()->GetController(0);				
				if (((controller->buttons_pressed() &(ABFW_SONY_CTRL_START)) == ABFW_SONY_CTRL_START)){					
					screen_manager_->PushScreen(new PauseScreen(platform_,shared_assets_));
					Pause(true);										
				}
				if (((controller->buttons_pressed() &(ABFW_SONY_CTRL_CIRCLE)) == ABFW_SONY_CTRL_CIRCLE)){
					follow_cam = !follow_cam;
				}
				spaceship_.UpdateInput(screen_manager_->GetTouchManager());

				float time_step = 1.0f / 60.0f;
				int32 velocity_iterations = 6;
				int32 position_iterations = 2;
				// Update world
				world_->Step(time_step, velocity_iterations, position_iterations);
								
				// Update sprites
				asteroid_manager_.Update(dt);
				planet_manager_.Update(dt);
				pickup_manager_.Update(dt);
				spaceship_.Update(dt);				
				
				ApplyGravityWellForces();

				if (portal_.is_entered()){
					spaceship_.BlowUp();
					state_ = kOverlayed;
					screen_manager_->PushScreen(new WinScreen(platform_, spaceship_.score()));
					Pause(true);
				}
				if (spaceship_.needs_deleted()){
					state_ = kOverlayed;
					screen_manager_->PushScreen(new LoserScreen(platform_));
					Pause(true);
				}
								
				break;
			}
		case kOverlayed:{
				spaceship_.Update(dt);				
#ifdef _PLATFORM_VITA	
				if (!loading_ &&thread_running_){
					LoadStopThread();
				}
#endif
				break;
			}
		case kHidden:
			break;
		default:
			break;
	}
}
void Level::Render(abfw::SpriteRenderer *sprite_renderer, abfw::Font *font){
	abfw::Matrix44 prev_projection;
	// Saving previous matrix from spriterenderer causes 
	prev_projection = sprite_renderer->projection_matrix();	
	abfw::Matrix44 projection_matrix;
	// follow camera
	if (follow_cam){
		projection_matrix = platform_.OrthographicFrustum(spaceship_.position().x - (platform_.width() / 2),
			spaceship_.position().x + (platform_.width() / 2),
			spaceship_.position().y - (platform_.height() / 2),
			spaceship_.position().y + (platform_.height() / 2),
			-1, 1);
	}
	else {
		// map to background, for level design hacky cheat
		projection_matrix = platform_.OrthographicFrustum(0,
			background_.width(),
			0,
			background_.height(),
			-1, 1);
	}
	switch(state_) {
		case kActive:			
			sprite_renderer->set_projection_matrix(projection_matrix);

			sprite_renderer->DrawSprite(background_);
			boundary_manager_.Render(sprite_renderer);			
			planet_manager_.Render(sprite_renderer);
			sprite_renderer->DrawSprite(portal_.Sprite());			
			pickup_manager_.Render(sprite_renderer);
			asteroid_manager_.Render(sprite_renderer);
			spaceship_.Render(sprite_renderer, font);			
			
			sprite_renderer->set_projection_matrix(prev_projection);

			spaceship_.RenderHud(sprite_renderer, font);			
			break;
		case kOverlayed:
			sprite_renderer->set_projection_matrix(projection_matrix);

			sprite_renderer->DrawSprite(background_);
			sprite_renderer->DrawSprite(portal_.Sprite());
			planet_manager_.Render(sprite_renderer);
			asteroid_manager_.Render(sprite_renderer);
			pickup_manager_.Render(sprite_renderer);
			spaceship_.Render(sprite_renderer, font);	
			sprite_renderer->set_projection_matrix(prev_projection);
			break;
		default:
			break;
	}
		
}

void Level::ApplyWellForce(Planet* planet, GameObject* gameobject){
	// planet affecting gameobject
	b2Vec2 vector_distance = planet->body()->GetPosition() - gameobject->body()->GetPosition();

	float distance = vector_distance.Length();
	// check within gravitional pull.				
	if (distance < planet->gravity_radius()) {
		float force = (planet->gravity()* gameobject->body()->GetMass()) / std::powf(distance, 2);
		vector_distance *= force;
		gameobject->body()->ApplyForceToCenter(vector_distance);
	}
}
void Level::ApplyGravityWellForces() {
	for(std::vector<Planet* >::iterator planet = planet_manager_.GetPlanets().begin(); planet != planet_manager_.GetPlanets().end(); planet++) {
		// pull asteroids
		for(std::vector<Asteroid* >::iterator asteroid = asteroid_manager_.GetAsteroids().begin(); asteroid != asteroid_manager_.GetAsteroids().end(); asteroid++) {			
			ApplyWellForce((*planet), (*asteroid));
		}
		// pull ship		
		ApplyWellForce((*planet), &spaceship_);
	}
}
