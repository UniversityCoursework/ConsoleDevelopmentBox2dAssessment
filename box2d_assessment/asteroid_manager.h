#ifndef _ASTEROID_MANAGER_H
#define _ASTEROID_MANAGER_H

#include <graphics/sprite_renderer.h>

#include <vector>

#include "asteroid.h"
#include "asteroid_spawner.h"

class AsteroidManager {
public:
	void CleanUp();

	void AddSpawner(AsteroidSpawner *spawner);
	void Update(float dt);
	void Render(abfw::SpriteRenderer *renderer);

	void AddAsteroid(b2World* world, abfw::Vector3 position, float radius, float collision_radius,
		b2Vec2 direction, abfw::Texture* texture, abfw::Texture * explosion_texture);
	
	std::vector<Asteroid* > &GetAsteroids();
private:
	std::vector<Asteroid* > asteroids_;
	typedef std::vector<Asteroid*>::iterator Asteroid_It;
	std::vector<AsteroidSpawner* > spawners_;
	typedef std::vector<AsteroidSpawner*>::iterator Spawner_It;
};

#endif
