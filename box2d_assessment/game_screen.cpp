#include "game_screen.h"

#include <assets/png_loader.h>
#include <graphics/image_data.h>
#include <graphics/texture.h>
#include <iostream>
#include <system\platform.h>

#include "screen_manager.h"
#include "loading_screen.h"

GameScreen::GameScreen(abfw::Platform & platform) :
	platform_(platform) {
	state_ = kActive;
	loading_ = false;	
	needs_deleted_ = false;	
#ifdef _PLATFORM_VITA
	thread_running_ = false;
#endif
}

GameScreen::~GameScreen() {	
}

void GameScreen::Pause(bool is_overlayed){
	if (is_overlayed) {
		state_ = kOverlayed;
	}
	else {
		state_ = kHidden;
	}
}

void GameScreen::Resume(){
	state_ = kActive;
}

abfw::Texture * GameScreen::LoadTextureFromPNG(const char * filename) {
	abfw::PNGLoader png_loader;
	abfw::ImageData image_data;

	png_loader.Load(filename, platform_, image_data);

	// check to see if image data has succesfully loaded
	if(image_data.image() == nullptr) {
		std::cout << filename << " failed to load." << std::endl;
		exit(-1);
		return nullptr;
	}
	return platform_.CreateTexture(image_data);
}

// allows multi threading of the load on vita
#ifdef _PLATFORM_VITA
#include <stdio.h>	// for printf
#include <ngs.h>	// for sce_ngs error messages

void GameScreen::LoadStartThread(SceKernelThreadEntry static_function){
	// pop up a loading screen and pause this screen.
	loading_ = true;
	thread_running_ = true;
	screen_manager_->PushScreen(new LoadingScreen(platform_));
	Pause(false);

	Int32 return_value;

	loader_thread_id_ = sceKernelCreateThread("Game Load Thread", static_function,
		SCE_KERNEL_HIGHEST_PRIORITY_USER,
		(8 * 1024), 0,									// not sure on the size required
		SCE_KERNEL_CPU_MASK_USER_1, SCE_NULL);
	if (loader_thread_id_ < 0) {
		printf("startLoadThread: sceKernelCreateThread() failed: %08x\n", loader_thread_id_);
		return;
	}

	GameScreen* game = this;
	return_value = sceKernelStartThread(loader_thread_id_, sizeof(game), &game);
	if (return_value < 0) {
		printf("startLoadThread: sceKernelStartThread() failed: %08x\n", return_value);
		return;
	}
	printf("startLoadThread: started LoadThread\n");

}
void GameScreen::LoadStopThread(){
	thread_running_ = false;
	Int32 return_value = SCE_NGS_OK;
	SceInt32 n32ThreadExitStatus;

	return_value = sceKernelWaitThreadEnd(loader_thread_id_, &n32ThreadExitStatus, SCE_NULL);
	if (return_value != SCE_NGS_OK) {
		printf("shutdown: sceKernelWaitThreadEnd() failed: 0x%08X\n", return_value);
		return;
	}
	printf("shutdown: LoadThread exit'd (exit status = %d)\n", n32ThreadExitStatus);	
	return;
}

#endif
