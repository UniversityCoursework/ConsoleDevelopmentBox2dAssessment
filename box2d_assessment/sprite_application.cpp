#include "sprite_application.h"

#include <graphics/sprite_renderer.h>
#include <system/platform.h>
#include <assets/png_loader.h>
#include <graphics/image_data.h>
#include <graphics/texture.h>
#include <iostream>
#include <input/touch_input_manager.h>

#include "main_menu.h"
#include "game_settings.h"

SpriteApplication::SpriteApplication(abfw::Platform& platform) :
	abfw::Application(platform),
	sprite_renderer_(NULL),
	controller_manager_(NULL),
	touch_manager_(NULL) {
}

SpriteApplication::~SpriteApplication() {
}

void SpriteApplication::Init() {
	// load the font to draw the on-screen text
	bool font_loaded = font_.Load("bin/comic_sans", platform_);
	if(!font_loaded) {
		std::cout << "Font failed to load." << std::endl;
		exit(-1);
	}

	// create a sprite renderer to draw the sprites
	sprite_renderer_ = platform_.CreateSpriteRenderer();

	// create the input controller manager to read controller input
	controller_manager_ = platform_.CreateSonyControllerInputManager();

	//create touch manager  		
	touch_manager_ = platform_.CreateTouchInputManager();		
	touch_manager_->EnablePanel(0);		// front panel, ui/menu
	touch_manager_->EnablePanel(1);		// back panel, ship controls

#ifdef _PLATFORM_VITA
	audio_manager_ = new abfw::AudioManagerVita();
	screen_manager_.Init(controller_manager_, touch_manager_, new GameSettings(), audio_manager_);
#else
	screen_manager_.Init(controller_manager_, touch_manager_,new GameSettings());
#endif

	//
	// initialise the sprite properties
	//
	
	screen_manager_.PushScreen(new MainMenu(platform_));

}

void SpriteApplication::CleanUp() {
	// free up the sprite renderer
	delete sprite_renderer_;
	sprite_renderer_ = NULL;

	delete touch_manager_;
	touch_manager_ = NULL;

	screen_manager_.CleanUp();
}

bool SpriteApplication::Update(float ticks) {
	// calculate the frame rate
	frame_rate_ = 1.0f / ticks;

	// update controller managers
	touch_manager_->Update();

	if(controller_manager_) {
		controller_manager_->Update();
		const abfw::SonyController* controller = controller_manager_->GetController(0);
		if(controller) {

		}
	}
	screen_manager_.Update(ticks);

	return true;
}

void SpriteApplication::Render() {
	// set up sprite renderer for drawing
	sprite_renderer_->Begin();

	//
	// draw all sprites here
	//
	screen_manager_.Render(sprite_renderer_, &font_);

	font_.RenderText(sprite_renderer_, abfw::Vector3(850.0f, 510.0f, -0.9f), 1.0f, 0xff00ffff, abfw::TJ_LEFT, "FPS: %.1f", frame_rate_);

	// tell sprite renderer that all sprites have been drawn
	sprite_renderer_->End();
}
