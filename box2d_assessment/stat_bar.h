#ifndef _STAT_BAR_H
#define _STAT_BAR_H

#include <graphics/sprite_renderer.h>
#include <graphics/sprite.h>
#include <graphics/font.h>

#include <sstream>
#include <string>

class StatBar {
public:
	StatBar(){};
	StatBar(abfw::Vector3 pos, float width, float height, UInt32 colour);
	// Updates the stat bar to be full to current/max %, repositions all the text and sprites.
	void Update(float current, float max);
	void Render(abfw::SpriteRenderer* renderer, abfw::Font *font);
protected:
	abfw::Sprite bar_;
	abfw::Sprite background_bar_;
	// store the max width
	float size_;
	float current_, max_;
	abfw::Vector3 text_position_;	
	
};


#endif