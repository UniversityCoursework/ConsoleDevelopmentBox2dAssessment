#ifndef _PLANET_MANAGER_H
#define _PLANET_MANAGER_H

#include <vector>
#include "planet.h"

class PlanetManager {

public:
	void CleanUp();

	void AddPlanet(Planet *planet);
	void Update(float dt);
	void Render(abfw::SpriteRenderer *renderer);

	std::vector<Planet* > &GetPlanets();
private:
	std::vector<Planet* > planets_;
	typedef std::vector<Planet*>::iterator Planet_it;
	
};

#endif
