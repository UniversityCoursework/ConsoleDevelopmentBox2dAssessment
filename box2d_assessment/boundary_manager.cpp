#include "boundary_manager.h"

#include "planet_manager.h"

void GameBoundaryManager::CleanUp() {
	// Edges
	for (GameBoundary_it boundary = game_boundaries_.begin(); boundary != game_boundaries_.end(); boundary++) {
		delete(*boundary);
		(*boundary) = nullptr;
	}
	game_boundaries_.clear();
}

void GameBoundaryManager::AddGameEdge(GameBoundary * edge) {
	game_boundaries_.push_back(edge);
}

void GameBoundaryManager::Render(abfw::SpriteRenderer * renderer) {
	for (GameBoundary_it boundary = game_boundaries_.begin(); boundary != game_boundaries_.end(); ++boundary) {
		(*boundary)->Render(renderer);
	}
}

