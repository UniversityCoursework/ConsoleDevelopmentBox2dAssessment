#ifndef _PLANET_H
#define _PLANET_H

#include <graphics/texture.h>
#include <maths/vector3.h>
#include <graphics/sprite.h>
#include <graphics/sprite_renderer.h>

#include "game_object.h"

class Planet : public GameObject{
public:
	Planet() {};
	~Planet();
	// @param gravity, how strong the graivtional pull is.
	// @param gravity_factor, multiplie of radius for reach of gravity.
	Planet(b2World* world, abfw::Vector3 position, float radius, float collision_radius, abfw::Texture* planet, float gravity, float gravity_radius, abfw::Texture* bg_radius);
	
	void Update(float dt) override;
	void Render(abfw::SpriteRenderer *sprite_renderer);

	float const gravity_radius() const { return gravity_radius_; }
	float const gravity() const { return gravity_; }
		
protected:
	abfw::Sprite planet_;
	abfw::Sprite planet_bg;

	float gravity_radius_;
	float gravity_;	

	void CreateStaticBody(b2World *world, float collision_radius);

};

#endif