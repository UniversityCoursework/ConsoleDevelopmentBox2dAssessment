#ifndef _SPAWNER_H
#define _SPAWNER_H

#include <maths\vector3.h>
#include <graphics/sprite_renderer.h>

#include <vector>

#include "asteroid.h"


class AsteroidSpawner {
public:
	AsteroidSpawner(b2World* world, abfw::Vector3 position, float radius, float collision_radius, b2Vec2 direction, abfw::Texture* texture, abfw::Texture * explosion_texture);
	
	void Update(float dt);
	
	// Delay between each spawn.
	inline void set_spawn_delay(const float num) { spawn_delay_ = num; }
	inline const float spawn_delay() const { return spawn_delay_; }

	inline void force_spawn() { spawn_timer_ = spawn_delay_; }

	void SetAsteroids(std::vector<Asteroid* > *asteroids) { asteroids_ = asteroids; }
		
protected:
	void Spawn();
	// Asteroid Variables
	b2World *world_;
	abfw::Vector3 position_;
	float radius_;
	float collision_radius_;
	b2Vec2 direction_;
	abfw::Texture* texture_;
	abfw::Texture* t_explosion_;
	
	float spawn_timer_;
	float spawn_delay_;

	std::vector<Asteroid* > *asteroids_;
	
};

#endif
