#include "menu_screen.h"

#include <input\touch_input_manager.h>
#include <system/platform.h>

#include "screen_manager.h"

const float MenuScreen::analog_menu_delay_ = 0.3f;

MenuScreen::MenuScreen(abfw::Platform& platform)
	: GameScreen(platform){
	menu_delay_timer_ = 0;
	valid_touch_ = false;
}
void MenuScreen::UpdateMenu(float dt){
	bool touch_used_ = false;

	// process input and update buttons
	const abfw::Touch& first_touch = screen_manager_->GetTouchManager()->GetTouch(0, 0);

	if ((first_touch.type == abfw::TT_NEW)){
		valid_touch_ = true;
	}
	// Touch Screen Menu Select	
	if ((first_touch.type == abfw::TT_NEW) || (first_touch.type == abfw::TT_ACTIVE)) {
		touch_used_ = true;
		for (int i = 0; i < buttons_.size(); i++) {
			if (buttons_[i].ContainsPoint(abfw::Vector3(first_touch.position.x, first_touch.position.y, 0))) {
				// set as current selection
				current_button_ = i;
			}
		}
	}
	else if (first_touch.type == abfw::TT_RELEASED&&valid_touch_) {
		touch_used_ = true;
		for (int i = 0; i < buttons_.size(); i++) {
			if (buttons_[i].ContainsPoint(abfw::Vector3(first_touch.position.x, first_touch.position.y, 0))) {
				// do button stuff
				UseButton(i);				
			}			
		}
		current_button_ = -1;
	}
	const abfw::SonyController* controller = screen_manager_->GetControllerManager()->GetController(0);	
	menu_delay_timer_ -= dt;
	if (controller&&!touch_used_){
		if (menu_delay_timer_<0){
			// analog sticks
			float left_axis_y = controller->left_stick_y_axis();
			float right_axis_y = controller->right_stick_y_axis();
			if ((left_axis_y > 0 || right_axis_y > 0)){
				current_button_++;
				menu_delay_timer_ = analog_menu_delay_;
			}
			if ((left_axis_y < 0 || right_axis_y < 0)){
				current_button_--;
				menu_delay_timer_ = analog_menu_delay_;
			}			
		}
		// D-Pad Menu Select
		if ((controller->buttons_pressed() &(ABFW_SONY_CTRL_UP)) == ABFW_SONY_CTRL_UP){
			current_button_--;
		}
		if ((controller->buttons_pressed() &(ABFW_SONY_CTRL_DOWN)) == ABFW_SONY_CTRL_DOWN){
			current_button_++;
		}
		// if x pressed select active button
		if (((controller->buttons_pressed() &(ABFW_SONY_CTRL_CROSS)) == ABFW_SONY_CTRL_CROSS)){
			for (int i = 0; i <buttons_.size(); i++) {
				if (buttons_[i].is_active()){
					UseButton(i);
					break;
				}
			}
		}
	}

	// check within buttons range
	if (current_button_ < -1){
		current_button_ = buttons_.size();
	}
	else if (current_button_ > static_cast<int>(buttons_.size())){
		current_button_ = 0;
	}

	// reset all buttons
	for (int i = 0; i < buttons_.size(); i++) {
		buttons_[i].set_is_active(false);
	}	
	if(current_button_ >= 0) {
		buttons_[current_button_].set_is_active(true);
	}
	
}